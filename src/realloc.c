/*
 *   Copyright Institute of Communication and Computer Systems (ICCS) 
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 */

/**
 * @file   src/realloc.c
 * @author Ioannis Koutras (joko@microlab.ntua.gr)
 * @date   September 2012
 *
 * @brief  Implementation of realloc() call.
 */

#include "dmmlib/dmmlib.h"

#include "dmmlib/debug.h"

#include "locks.h"
#include "default_rb.h"
#include "dmmlib_trace.h"
#include "other.h"
#include "padding.h"
#include "statistics.h"

#include "release_memory.h"
#include <sys/mman.h>
#include <inttypes.h>

#ifdef PAGESIZE_ALIGN
#include <unistd.h> /* for pagesize */
#endif /* PAGESIZE_ALIGN */

#include "memcpy.h"


void * realloc(void *ptr, size_t size) {
    raw_block_header_t *owner_raw_block;
    void *return_ptr = NULL;

    if(ptr == NULL) {
        return malloc(size);
    }

    if(size == 0) {
        free(ptr);
        return NULL;
    }

    owner_raw_block = find_raw_block_owner(systemallocator.rb_head, ptr);

    if(owner_raw_block != NULL) {

        DEFAULT_RB_T *encapsulated_rb = (DEFAULT_RB_T *)
            ((uintptr_t) owner_raw_block + sizeof(raw_block_header_t));
        
        return_ptr = dmmlib_realloc(encapsulated_rb, ptr, size);

    } else { /* This has to be a big block */

        // The new size is checked and if it is smaller than the one from the
        // original request, the remaining memory is simply released. If it is
        // greater, a new big block is initialized, data is copied there and the
        // old big block gets de-allocated.

        owner_raw_block = (raw_block_header_t *)
            ((uintptr_t) ptr - sizeof(raw_block_header_t));

        size_t full_size = sizeof(raw_block_header_t) + req_padding(size);

#ifdef PAGESIZE_ALIGN
        size_t pagesize = (size_t) sysconf(_SC_PAGESIZE);
        full_size = pagesize * ((full_size + pagesize - 1) / pagesize);
#endif /* PAGESIZE_ALIGN */

        if(full_size < owner_raw_block->size) {
            /* The re-allocation can take place inside the same big block */

            size_t remaining_size = owner_raw_block->size - full_size;

#ifdef REQUEST_SIZE_INFO
            UPDATE_GLOBAL_STATS(REALLOC_LT, owner_raw_block->requested_size -
                    size);
#else /* REQUEST_SIZE_INFO */
            UPDATE_GLOBAL_STATS(REALLOC_LT);
#endif /* REQUEST_SIZE_INFO */

            LOCK_RAW_BLOCK(owner_raw_block);
#ifdef REQUEST_SIZE_INFO
            owner_raw_block->requested_size = size;
#endif /* REQUEST_SIZE_INFO */
            UNLOCK_RAW_BLOCK(owner_raw_block);

            // FIXME This is pagesize and mmap-specific
#ifdef PAGESIZE_ALIGN
            if(remaining_size > pagesize) {
                remaining_size = pagesize * ((remaining_size + pagesize - 1) /
                        pagesize);

                LOCK_RAW_BLOCK(owner_raw_block);
                owner_raw_block->size = full_size;
                UNLOCK_RAW_BLOCK(owner_raw_block);

#ifdef WITH_ALLOCATOR_STATS
                LOCK_GLOBAL();
                systemallocator.dmm_stats.total_mem_allocated -= remaining_size;
                UNLOCK_GLOBAL();
#endif /* WITH_ALLOCATOR_STATS */

                munmap((void *)((uintptr_t) owner_raw_block +
                            owner_raw_block->size), remaining_size);
            }
#endif /* PAGESIZE_ALIGN */

            return_ptr = ptr;

        } else if(full_size == owner_raw_block->size) {
            /* no need to make modifications */
            return_ptr = ptr;
        } else { /* We have to create a new big block */

            raw_block_header_t *new_block = create_raw_block(full_size,
                    BIGBLOCK);

            if(new_block == NULL) {
                return_ptr = NULL;
            } else {

                return_ptr = (void *)
                    ((uintptr_t) new_block + sizeof(raw_block_header_t));

                memcpy(return_ptr, ptr,
                        owner_raw_block->size - sizeof(raw_block_header_t));

#ifdef WITH_DEBUG
                LOCK_GLOBAL();
                LOCK_RAW_BLOCK(owner_raw_block);
                SLIST_REMOVE(&systemallocator.bb_head, owner_raw_block,
                        raw_block_header_s, pointers);
                UNLOCK_RAW_BLOCK(owner_raw_block);
                LOCK_RAW_BLOCK(new_block);
                SLIST_INSERT_HEAD(&systemallocator.bb_head,
                        new_block, pointers);
                UNLOCK_RAW_BLOCK(new_block);
                UNLOCK_GLOBAL();
#endif /* WITH_DEBUG */

#ifdef REQUEST_SIZE_INFO
                UPDATE_GLOBAL_STATS(REALLOC_GT, size -
                        owner_raw_block->requested_size);
#else /* REQUEST_SIZE_INFO */
                UPDATE_GLOBAL_STATS(REALLOC_GT);
#endif /* REQUEST_SIZE_INFO */

#ifdef WITH_ALLOCATOR_STATS
                LOCK_GLOBAL();
                systemallocator.dmm_stats.total_mem_allocated += new_block->size
                    - owner_raw_block->size;
                UNLOCK_GLOBAL();
#endif /* WITH_ALLOCATOR_STATS */

                release_memory(owner_raw_block);
            }
        }
    }

    MEM_TRACE("dmmlib - r %p %p %zu\n", ptr, return_ptr, size);

    return return_ptr;

}
