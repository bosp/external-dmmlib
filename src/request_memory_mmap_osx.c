/*
 *   Copyright 2012 Institute of Communication and Computer Systems (ICCS) 
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 */

/**
 * \file   request_memory_mmap_osx.c
 * \author Ioannis Koutras
 * \date   August, 2012
 * \brief  Request additional memory space via mmap() in OS X.
 */

#include "request_memory.h"
#include <sys/mman.h>

#include <mach/vm_statistics.h> /* for VM_MAKE_TAG */

void *request_memory(size_t size) {	
    void *zone;

    zone = mmap(0, size, PROT_READ | PROT_WRITE, MAP_ANON | MAP_PRIVATE,
            VM_MAKE_TAG(VM_MEMORY_MALLOC), (off_t) 0);

    if(zone == MAP_FAILED) {
        return NULL;
    } else {
        return zone;
    }
}
