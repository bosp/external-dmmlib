/*
 *   Copyright Institute of Communication and Computer Systems (ICCS) 
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 */

/**
 * @file   knobs.c
 * @author Ioannis Koutras (joko@microlab.ntua.gr)
 * @date   November 2012
 * @brief  Knobs initialization and configuration routines.
 */

#include <dmmlib/config.h>
#include "dmmlib/dmmlib.h"

#include <inttypes.h>

#include "locks.h"

uint32_t dmm_set_knobs(dmm_knobs_t *conf) {
    LOCK_GLOBAL();
    systemallocator.dmm_knobs.p.sys_alloc_size = conf->p.sys_alloc_size;
#ifdef GOOD_FIT
    systemallocator.dmm_knobs.p.fit_percentage = conf->p.fit_percentage;
#endif /* GOOD_FIT_PERCENTAGE */
#ifdef COALESCING_VARIABLE
    systemallocator.dmm_knobs.p.max_coal_size = conf->p.max_coal_size;
#endif /* COALESCING_VARIABLE */
#ifdef SPLITTING_VARIABLE
    systemallocator.dmm_knobs.p.min_split_size = conf->p.min_split_size;
#endif /* SPLITTING_VARIABLE */
    UNLOCK_GLOBAL();
    return 0;
}

void dmm_notify_cycle(void) {
    return;
}
