/*
 *   Copyright Institute of Communication and Computer Systems (ICCS) 
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 */

/**
 * @file   src/memalign.c
 * @author Ioannis Koutras (joko@microlab.ntua.gr)
 * @date   November 2012
 *
 * @brief  Implementation of memory-aligned allocation calls for
 * freelist-organized raw blocks.
 */

#include "dmmlib/memalign.h"

#include <inttypes.h>
#include <sys/mman.h>
#include <assert.h>

#include "dmmlib/config.h"
#include "dmmlib/dmmlib.h"

#include "memcpy.h"

#include "default_rb.h"
#include "dmmlib_trace.h"
#include "locks.h"
#include "statistics.h"

#ifdef BITMAP_RB_ONLY
#error Current memory-aligned allocation implementation supports only \
freelist-organized raw blocks.
#endif /* BITMAP_RB_ONLY */

/** The function posix_memalign() allocates size bytes and places the address of
 * the allocated memory in *memptr. The address of the allocated memory will be
 * a multiple of alignment, which must be a power of two and a multiple of
 * sizeof(void *). If size is 0, then posix_memalign() returns either NULL.
 */
int posix_memalign(void **memptr, size_t alignment, size_t size) {
    DEFAULT_RB_T *encapsulated_rb;

    *memptr = NULL;

    /* Input check */
    if(size == 0 || (alignment & 1) != 0 || alignment % sizeof(void *) != 0) {
        return 0;
    }

    raw_block_header_t *raw_block;

    /* Search the available freelist-organized raw blocks for a block whose size
     * is size + alignment - 1 */
    SLIST_FOREACH(raw_block, &systemallocator.rb_head, pointers) {
        LOCK_RAW_BLOCK(raw_block);
        encapsulated_rb = (freelist_rb_t *) 
            ((uintptr_t) raw_block + sizeof(raw_block_header_t));
        *memptr = freelist_memalign(encapsulated_rb, alignment, size);
        UNLOCK_RAW_BLOCK(raw_block);
    }

    if(*memptr == NULL) {

        if( 2 * (size + alignment - 1)  > SYS_ALLOC_SIZE -
                    sizeof(raw_block_header_t) - sizeof(freelist_rb_t)) {

            /* A big block has to be created */

            *memptr = (void *)create_raw_block(size + alignment - 1 +
                    sizeof(raw_block_header_t), BIGBLOCK);
            if(*memptr != NULL) {

                *memptr = (void *)((uintptr_t) *memptr +
                        sizeof(raw_block_header_t));

                /* Check if alignment is needed */
                if(((uintptr_t) *memptr) % alignment != 0) {
                    size_t padding = (- (size_t) *memptr) & (alignment - 1);

                    /* Copy the raw block's header to the new location */
                    memcpy((void *)((uintptr_t) *memptr
                                - sizeof(raw_block_header_t) + padding),
                                (void *)((uintptr_t) *memptr
                                - sizeof(raw_block_header_t)),
                                sizeof(raw_block_header_t)
                           );

                    /* Update *memptr */
                    *memptr = (void *)((uintptr_t) *memptr + padding);

                    /* Update big block's size and requested size */
                    raw_block_header_t *aligned_header =
                        (raw_block_header_t *)((uintptr_t) *memptr -
                                sizeof(raw_block_header_t));
                    LOCK_RAW_BLOCK(aligned_header);
                    aligned_header->size -= padding;
#ifdef REQUEST_SIZE_INFO
                    aligned_header->requested_size = size;
#endif /* REQUEST_SIZE_INFO */
                    UNLOCK_RAW_BLOCK(aligned_header);

                }

#ifdef WITH_DEBUG
                LOCK_GLOBAL();
                LOCK_RAW_BLOCK(((raw_block_header_t *) ((uintptr_t) *memptr -
                                sizeof(raw_block_header_t))));
                SLIST_INSERT_HEAD(&systemallocator.bb_head,
                        (raw_block_header_t *) ((uintptr_t) *memptr -
                            sizeof(raw_block_header_t)), pointers);
                UNLOCK_RAW_BLOCK(((raw_block_header_t *) ((uintptr_t) *memptr -
                                sizeof(raw_block_header_t))));
                UNLOCK_GLOBAL();
#endif /* WITH_DEBUG */

            }
        } else { /* Create a new raw block and use it */
            raw_block = create_raw_block((size_t) SYS_ALLOC_SIZE,
                    DEFAULT_RB_TYPE);
            if(raw_block != NULL) {
                LOCK_GLOBAL();
                LOCK_RAW_BLOCK(raw_block);
                SLIST_INSERT_HEAD(&systemallocator.rb_head, raw_block, pointers);
                UNLOCK_GLOBAL();

                encapsulated_rb = (DEFAULT_RB_T *)
                    ((uintptr_t) raw_block + sizeof(raw_block_header_t));
                *memptr = freelist_memalign(encapsulated_rb, alignment, size);
                UNLOCK_RAW_BLOCK(raw_block);
            }
        }
    }

    if(*memptr != NULL) {
        /* Assert that the returned address is a multiple of alignment */
        assert((uintptr_t) *memptr % alignment == 0);

#ifdef REQUEST_SIZE_INFO
        UPDATE_GLOBAL_STATS(MEMALIGN, size);
#else /* REQUEST_SIZE_INFO */
        UPDATE_GLOBAL_STATS(MEMALIGN);
#endif /* REQUEST_SIZE_INFO */
    }

    MEM_TRACE("dmmlib - ma %p %zu %zu\n", *memptr, alignment, size);

    return 0;
}
