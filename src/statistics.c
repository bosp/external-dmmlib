/*
 *   Copyright Institute of Communication and Computer Systems (ICCS) 
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 */

/**
 * @file   statistics.c
 * @author Ioannis Koutras (joko@microlab.ntua.gr)
 * @date   September 2012
 * @brief  Statistics function implementation.
 */

#include "statistics.h"

#include "dmmlib/dmmlib.h"

#include "dmmlib_trace.h"

/** Updates a dmmstats_t variable
 *
 * @param dmm_stats Pointer to the dmmstats_t variable.
 * @param event     The event type, could be MALLOC or FREE.
 * */
#ifdef REQUEST_SIZE_INFO
 /**
  * @param mem_req   Requested memory size info.
  */
#endif /* REQUEST_SIZE_INFO */
void update_stats
    ( dmmstats_t *dmm_stats
    , dmm_event_type event
#ifdef REQUEST_SIZE_INFO
    , size_t mem_req
#endif /* REQUEST_SIZE_INFO */
    )
{
    switch(event) {
        case MALLOC :
#ifdef REQUEST_SIZE_INFO
            dmm_stats->total_mem_requested += mem_req;
#endif /* REQUEST_SIZE_INFO */
            dmm_stats->live_objects++;
            dmm_stats->num_malloc++;
            break;
        case FREE :
#ifdef REQUEST_SIZE_INFO
            dmm_stats->total_mem_requested -= mem_req;
#endif /* REQUEST_SIZE_INFO */
            dmm_stats->live_objects--;
            dmm_stats->num_free++;
            break;
#ifdef WITH_REALLOC
        case REALLOC_LT :
#ifdef REQUEST_SIZE_INFO
            dmm_stats->total_mem_requested -= mem_req;
#endif /* REQUEST_SIZE_INFO */
            dmm_stats->num_realloc++;
            break;
        case REALLOC_GT :
#ifdef REQUEST_SIZE_INFO
            dmm_stats->total_mem_requested += mem_req;
#endif /* REQUEST_SIZE_INFO */
            dmm_stats->num_realloc++;
            break;
#endif /* WITH_REALLOC */
#ifdef WITH_MEMALIGN
        case MEMALIGN :
#ifdef REQUEST_SIZE_INFO
            dmm_stats->total_mem_requested += mem_req;
#endif /* REQUEST_SIZE_INFO */
            dmm_stats->live_objects++;
            dmm_stats->num_memalign++;
            break;
#endif /* WITH_MEMALIGN */
        default :
            break;
    }

    STATS_TRACE("dmmlib - ms all %zu\n", dmm_stats->total_mem_allocated);
#ifdef REQUEST_SIZE_INFO
    STATS_TRACE("dmmlib - ms req %zu\n", dmm_stats->total_mem_requested);
#endif /* REQUEST_SIZE_INFO */

}

#ifdef REQUEST_SIZE_INFO
/** Returns the total memory currently requested by the application. */
size_t get_total_requested_memory(void) {
    return systemallocator.dmm_stats.total_mem_requested;
}
#endif /* REQUEST_SIZE_INFO */

size_t get_total_allocated_memory(void) {
    return systemallocator.dmm_stats.total_mem_allocated;
}

long double get_utilization_index(void) {
    if(systemallocator.dmm_stats.total_mem_allocated != 0) {
        return (long double) systemallocator.dmm_stats.total_mem_requested /
            systemallocator.dmm_stats.total_mem_allocated;
    } else {
        return 1.0;
    }
}
