/*
 *   Copyright Institute of Communication and Computer Systems (ICCS) 
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 */

/**
 * @file   src/free.c
 * @author Ioannis Koutras (joko@microlab.ntua.gr)
 * @date   September 2012
 *
 * @brief  Implementation of the free() call.
 */

#include <inttypes.h>

#include "dmmlib/dmmlib.h"

#include "default_rb.h"
#include "dmmlib_trace.h"
#include "locks.h"
#include "other.h"
#include "release_memory.h"
#include "statistics.h"

void free(void *ptr) {
    raw_block_header_t *owner_raw_block;

    if(ptr == NULL) {
        return;
    }

#if defined PARSE_ENV && defined WITH_MEM_TRACE
    if(__builtin_expect (systemallocator.initialized, true)) {
#endif /* PARSE_ENV && WITH_MEM_TRACE */
    MEM_TRACE("dmmlib - f %p\n", ptr);
#if defined PARSE_ENV && defined WITH_MEM_TRACE
    }
#endif /* PARSE_ENV && WITH_MEM_TRACE */

    owner_raw_block = find_raw_block_owner(systemallocator.rb_head, ptr);

    if(owner_raw_block != NULL) {

        DEFAULT_RB_T *encapsulated_rb = (DEFAULT_RB_T *)
            ((uintptr_t) owner_raw_block + sizeof(raw_block_header_t));

        LOCK_RAW_BLOCK(owner_raw_block);
        dmmlib_free(encapsulated_rb, ptr);
        UNLOCK_RAW_BLOCK(owner_raw_block);

    } else { // It has to be a BIGBLOCK, just munmap it
        owner_raw_block = (raw_block_header_t *)
            ((uintptr_t) ptr - sizeof(raw_block_header_t));

#ifdef WITH_DEBUG
        LOCK_GLOBAL();
        SLIST_REMOVE(&systemallocator.bb_head, owner_raw_block,
                raw_block_header_s, pointers);
        UNLOCK_GLOBAL();
#endif /* WITH_DEBUG */

#ifdef WITH_ALLOCATOR_STATS
        LOCK_GLOBAL();
        update_stats
            ( &systemallocator.dmm_stats
            , FREE
#ifdef REQUEST_SIZE_INFO
            , owner_raw_block->requested_size
#endif /* REQUEST_SIZE_INFO */
            );
        systemallocator.dmm_stats.total_mem_allocated -= owner_raw_block->size;
        UNLOCK_GLOBAL();
#endif /* WITH_ALLOCATOR_STATS */

        release_memory(owner_raw_block);
    }
}
