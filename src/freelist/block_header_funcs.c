/*
 *   Copyright Institute of Communication and Computer Systems (ICCS) 
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 */

#include "freelist/block_header_funcs.h"

#include <inttypes.h>

block_header_t * get_header(void *ptr) {
    return (block_header_t *) ((uintptr_t) ptr - HEADER_SIZE);
}

size_t get_size(block_header_t *ptr) {
    return ptr->size >> 1;
}

#ifdef REQUEST_SIZE_INFO
void set_requested_size(block_header_t *ptr, size_t size) {
    ptr->requested_size = size;
}

size_t get_requested_size(block_header_t *ptr) {
    return ptr->requested_size;
}
#endif /* REQUEST_SIZE_INFO */

size_t get_size_availability(block_header_t *ptr) {
    return ptr->size;
}

void set_size_and_free(freelist_rb_t *raw_block, block_header_t *ptr,
        size_t size) {
    block_header_t *next_block_header;

    ptr->size = size << 1;

    next_block_header = get_dlnext(raw_block, ptr);
    if(next_block_header != NULL) {
        next_block_header->previous_size = size << 1;
    }
}

void set_size_and_used(freelist_rb_t *raw_block, block_header_t *ptr,
        size_t size) {
    block_header_t *next_block_header;

    ptr->size = size << 1;
    ptr->size |= 1;

    next_block_header = get_dlnext(raw_block, ptr);
    if(next_block_header != NULL) {
        next_block_header->previous_size = ptr->size;
    }
}

void mark_used(freelist_rb_t *raw_block, block_header_t *ptr) {
    block_header_t *next_block_header;

    ptr->size |= 1;

    next_block_header = get_dlnext(raw_block, ptr);
    if(next_block_header != NULL) {
        next_block_header->previous_size |= 1;
    }
}

void mark_free(freelist_rb_t *raw_block, block_header_t *ptr) {
    block_header_t *next_block_header;
    
    ptr->size &= (~ ((size_t) 0x1));

    next_block_header = get_dlnext(raw_block, ptr);
    if(next_block_header != NULL) {
        next_block_header->previous_size &= (~ ((size_t) 0x1));
    }    
}

bool is_free(block_header_t *ptr) {
    if(ptr->size & (size_t) 1) {
        return false;
    } else {
        return true;
    }
}

bool is_previous_free(block_header_t *ptr) {
    if(ptr->previous_size & (size_t) 1) {
        return false;
    } else {
        return true;
    }
}

void set_previous_size_availability(block_header_t *ptr,
        size_t previous_size_availability) {
    ptr->previous_size = previous_size_availability;
}

size_t get_previous_size(block_header_t *ptr) {
    return ptr->previous_size >> 1;
}

size_t get_previous_size_availability(block_header_t *ptr) {
    return ptr->previous_size;
}

block_header_t * get_dlprevious(block_header_t *ptr) {
    return (block_header_t *) ((uintptr_t) ptr - HEADER_SIZE -
            get_previous_size(ptr));
}

block_header_t * get_dlnext(freelist_rb_t *raw_block, block_header_t *ptr) {
    if(raw_block->border_ptr != ptr) {
        return (block_header_t *) ((uintptr_t) ptr + get_size(ptr) +
                HEADER_SIZE);
    } else {
        return (block_header_t *) NULL;
    }
}

