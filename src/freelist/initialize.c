/*
 *   Copyright Institute of Communication and Computer Systems (ICCS)
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 */

/**
 * @file   freelist/initialize.c
 * @author Ioannis Koutras (joko@microlab.ntua.gr)
 * @date   October 2012
 *
 * @brief  Implementation of free-list initialization.
 */

#include "dmmlib/lists.h"
#include "dmmlib/freelist/initialize.h"
#include "dmmlib/freelist/freelist_rb.h"

void initialize_freelist(void *address, size_t available_size) {
    freelist_rb_t *fl_rb = (freelist_rb_t *)address;

    fl_rb->remaining_size = available_size - sizeof(freelist_rb_t);
    fl_rb->border_ptr = NULL;
    
    SLIST_INIT(&fl_rb->fl_head);

    return;
}
