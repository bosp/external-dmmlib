/*
 *   Copyright Institute of Communication and Computer Systems (ICCS) 
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 */

/**
 * @file   freelist/malloc.c
 * @author Ioannis Koutras
 * @date   September 2012
 * @brief  malloc() implementation for freelist-organised raw blocks
 */


#include <inttypes.h>

#include "dmmlib/config.h"

#include "dmmlib/freelist/freelist.h"

#include "dmmlib_trace.h"
#include "padding.h"
#include "statistics.h"

#include "freelist/block_header_funcs.h"
#include "freelist/fitting_policy.h"
#include "freelist/split.h"

/** Tries to allocate memory from a specific free-list organized raw block.
 * @param raw_block The pointer of the freelist-organised raw block.
 * @param size      The requested size.
 * @retval          The address of the returned memory space.
 */
void * freelist_malloc(freelist_rb_t *raw_block, size_t size) {
    block_header_t *ptr;
    size_t allocation_size, previous_size, previous_size_availability;

    ptr = SEARCH_LIST(raw_block, size);

    if(ptr != NULL) {
        SET_REQUESTED_SIZE(ptr, size);

        /* Try to split */
        SPLIT(raw_block, ptr, size);

        mark_used(raw_block, ptr);
    } else {

        allocation_size = req_padding(size) + HEADER_SIZE;

        if(allocation_size <= raw_block->remaining_size) {
            if(raw_block->border_ptr == NULL) {
                previous_size_availability = 1; // Occupied and of 0 size
                ptr = (block_header_t *)((uintptr_t) raw_block +
                        sizeof(freelist_rb_t));
            } else {
                previous_size = get_size(raw_block->border_ptr);
                previous_size_availability =
                    get_size_availability(raw_block->border_ptr);
                ptr = (block_header_t *)((uintptr_t) raw_block->border_ptr +
                        HEADER_SIZE + previous_size);
            }

            // Update raw block metadata
            raw_block->remaining_size -= allocation_size;
            raw_block->border_ptr = ptr;

            // Update block metadata
            set_size_and_used(raw_block, ptr, req_padding(size));
            set_previous_size_availability(ptr, previous_size_availability);
            SET_REQUESTED_SIZE(ptr, size);
        }
    }

    if(ptr != NULL) {
        return (void *)((uintptr_t) ptr + HEADER_SIZE);
    } else {
        return NULL;
    }
}
