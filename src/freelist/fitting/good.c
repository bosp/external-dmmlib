/*
 *   Copyright Institute of Communication and Computer Systems (ICCS) 
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 */

#include <dmmlib/config.h>

#ifdef WITH_KNOBS
#include "dmmlib/dmmlib.h"
#endif /* WITH_KNOBS */

#include "freelist/fitting/good.h"
#include "freelist/ordering_policy.h"
#include "freelist/block_header_funcs.h"

block_header_t * fl_good_fit(freelist_rb_t *raw_block, size_t requested_size) {
    block_header_t *current_block, *previous_block;
    block_header_t *good_block, *good_previous_block;
    size_t good_size, block_size, ideal_size;

    previous_block = NULL;
    good_previous_block = NULL;
    good_block = NULL;
    good_size = (size_t) -1; /* SIZE_MAX */
#ifdef WITH_KNOBS
    ideal_size = (size_t) ((double) requested_size /
            systemallocator.dmm_knobs.p.fit_percentage);
#else /* WITH_KNOBS */
    ideal_size = (size_t) ((double) requested_size / GOOD_FIT_PERCENTAGE);
#endif /* WITH_KNOBS */
    
    SLIST_FOREACH(current_block, &raw_block->fl_head, pointers) {
        block_size = get_size(current_block);
        if(block_size >= requested_size) {
            if(block_size < good_size) {
                good_block = current_block;
                good_size = block_size;
                good_previous_block = previous_block;
                /* If the requested size is found, there is no need to keep
                 * searching for a better sized block */
                if(good_size <= ideal_size) {
                    break;
                }
            }
        }
        previous_block = current_block;
    } 

    /* Remove the block from the list */
    /* Note: remove_block() is not used because the block is already found */
    if(good_block != NULL) {
        if((raw_block->fl_head).slh_first == good_block) {
            REMOVE_FSLLIST_HEAD(raw_block);
        } else {
#ifdef FIFO_ORDERED
            if(raw_block->fl_tail == good_block) {
                raw_block->fl_tail = good_previous_block;
            }
#endif /* FIFO_ORDERED */
            good_previous_block->pointers.sle_next =
                good_block->pointers.sle_next;
        }
    }

    return good_block;
}
