/*
 *   Copyright Institute of Communication and Computer Systems (ICCS) 
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 */

#include "freelist/fitting/exact.h"
#include "freelist/ordering_policy.h"
#include "freelist/block_header_funcs.h"

/**
 * \details In order to remove a block from a singly linked list, we need to
 * keep track of the previous block as well: The previous block must point to
 * the current block's next block once the current one is removed.
 */
block_header_t * fl_exact_fit(freelist_rb_t *raw_block, size_t requested_size) {
    block_header_t *current_block, *previous_block, *ptr;

    ptr = NULL;
    previous_block = NULL;

    SLIST_FOREACH(current_block,&raw_block->fl_head, pointers) {
        if(get_size(current_block) == requested_size) {
            if((raw_block->fl_head).slh_first == current_block) {
                REMOVE_FSLLIST_HEAD(raw_block);
            } else {
#ifdef FIFO_ORDERED
                if(raw_block->fl_tail == current_block) {
                    raw_block->fl_tail = previous_block;
                }
#endif /* FIFO_ORDERED */
                previous_block->pointers.sle_next =
                    current_block->pointers.sle_next;
            }
            ptr = current_block;
            break;
        }
        previous_block = current_block;
    }

    return ptr;
}
