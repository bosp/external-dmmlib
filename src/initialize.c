/*
 *   Copyright Institute of Communication and Computer Systems (ICCS)
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 */

/**
 * @file   src/initialize.c
 * @author Ioannis Koutras (joko@microlab.ntua.gr)
 * @date   December 2012
 * @brief  System allocator's global object initializer.
 */

#include <dmmlib/config.h>
#include "dmmlib/dmmlib.h"

/** Global variable storing allocator's settings */
allocator_t systemallocator =
    { .rb_head = {NULL}
#ifdef WITH_DEBUG
    , .bb_head = {NULL}
#endif /* WITH_DEBUG */
#ifdef HAVE_LOCKS
    , .creation_mutex = PTHREAD_MUTEX_INITIALIZER
#endif /* HAVE_LOCKS */
#ifdef WITH_KNOBS
    , .dmm_knobs =
            { .p = 
                { .sys_alloc_size = SYS_ALLOC_SIZE
#ifdef GOOD_FIT
                , .fit_percentage = GOOD_FIT_PERCENTAGE
#endif /* GOOD_FIT */
#ifdef COALESCING_VARIABLE
                , .max_coal_size = MAX_COAL_SIZE
#endif /* COALESCING_VARIABLE */
#ifdef SPLITTING_VARIABLE
                , .min_split_size = MIN_SPLIT_SIZE
#endif /* SPLITTING_VARIABLE */
                }
            }
#endif /* WITH_KNOBS */
#ifdef WITH_ALLOCATOR_STATS
    ,   {
#ifdef REQUEST_SIZE_INFO
          0,
#endif /* REQUEST_SIZE_INFO */
          0
        , 0
        , 0
        , 0
        , 0
#ifdef WITH_MEMALIGN
        , 0
#endif /* WITH_MEMALIGN */
        }
#endif /* WITH_ALLOCATOR_STATS */
#ifdef PARSE_ENV
#if defined  WITH_MEM_TRACE || defined WITH_STATS_TRACE || defined WITH_DEBUG
    , .initialized = false
#endif /* WITH_MEM_TRACE || WITH_STATS_TRACE || WITH_DEBUG */
#endif /* PARSE_ENV */
};

