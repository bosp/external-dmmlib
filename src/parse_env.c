/*
 *   Copyright Institute of Communication and Computer Systems (ICCS) 
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 */

/**
 * @file   parse_env.c
 * @author Ioannis Koutras (joko@microlab.ntua.gr)
 * @date   November 2012
 * @brief  Parse the DMMLIB_OPTS enviroment variable.
 */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "dmmlib/config.h"
#include "dmmlib/dmmlib.h"
#include "dmmlib_trace.h"

__attribute__((constructor)) void parse_env(void);

/** Parses the DMMLIB_OPTS enviroment variable. */
void parse_env(void) {
    const char* env;

#ifdef WITH_MEM_TRACE
    env = getenv("DMMLIB_MEM_TRACE");

    if(env == NULL) {
        mem_fd = stderr;
    } else {
        mem_fd = fopen(env, "a+");
    }
#endif /* WITH_MEM_TRACE */

#ifdef WITH_STATS_TRACE
    env = getenv("DMMLIB_STATS_TRACE");

    if(env == NULL) {
        stats_fd = stderr;
    } else {
        stats_fd = fopen(env, "a+");
    }
#endif /* WITH_STATS_TRACE */

#ifdef WITH_DEBUG
    env = getenv("DMMLIB_DBG_TRACE");

    if(env == NULL) {
        dbg_fd = stderr;
    } else {
        dbg_fd = fopen(env, "a+");
    }
#endif /* WITH_DEBUG */

    systemallocator.initialized = true;

    return;
}

#if defined WITH_MEM_TRACE || defined WITH_STATS_TRACE || defined WITH_DEBUG
__attribute__((destructor)) void close_trace_files(void);

/** Closes the opened trace files. */
void close_trace_files(void) {
#ifdef WITH_MEM_TRACE
    if(mem_fd != stderr) {
        fclose(mem_fd);
    }
#endif /* WITH_MEM_TRACE */
#ifdef WITH_STATS_TRACE
    if(stats_fd != stderr) {
        fclose(stats_fd);
    }
#endif /* WITH_STATS_TRACE */
#ifdef WITH_DEBUG
    if(dbg_fd != stderr) {
        fclose(dbg_fd);
    }
#endif /* WITH_DEBUG */
    return;
}
#endif /* WITH_MEM_TRACE || WITH_STATS_TRACE || WITH_DEBUG */
