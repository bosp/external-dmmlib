/*
 *   Copyright Institute of Communication and Computer Systems (ICCS) 
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 */

/**
 * @file   bitmap/realloc.c
 * @author Ilias Pliotas, Ioannis Koutras
 * @date   September 2012
 * @brief  realloc() implementation for bitmap-organized raw blocks
 */

#include "bitmap/bitmap.h"
#include "dmmlib/dmmlib.h"

#include "memcpy.h"

#include "locks.h"

/**
 * Reallocates a memory block from a bitmap-organized raw block
 *
 * @param  raw_block The pointer of the bitmap raw block.
 * @param  ptr       The pointer of the memory block to be re-allocated.
 * @param  req_size  The requested memory size.
 * @retval           The address to serve the request.
 * @retval NULL      No available memory space.
 */
void * bitmap_realloc(bitmap_rb_t *raw_block, void * ptr,
        size_t req_size) { 
    void *ret;
    chunk_header_t *chunk_header;
    size_t old_size;

    chunk_header = (chunk_header_t *)((uintptr_t) ptr - CHUNK_HDR_SIZE);
    old_size = chunk_header->num_of_cells * raw_block->bytes_per_cell -
        CHUNK_HDR_SIZE;

    if(old_size >= req_size) {
        ret = ptr;
        // TODO Free possibly unneeded cells
    } else {

#ifdef HAVE_LOCKS
        raw_block_header_t *rb;
        rb = (raw_block_header_t *)
            ((uintptr_t) raw_block - sizeof(raw_block_header_t));
#endif /* HAVE_LOCKS */

        /* Try initially to allocate space from the same raw block */
        LOCK_RAW_BLOCK(rb);
        ret = bitmap_malloc(raw_block, req_size);
        UNLOCK_RAW_BLOCK(rb);

        /* If no space can be found, try your luck in other raw blocks */
        if(ret == NULL) {
            ret = malloc(req_size);
        }

        if(ret != NULL) {
            memcpy(ret, ptr, old_size);
            LOCK_RAW_BLOCK(rb);
            bitmap_free(raw_block, ptr);
            UNLOCK_RAW_BLOCK(rb);
        }
    }

    return ret;
}
