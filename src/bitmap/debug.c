/*
 *   Copyright Institute of Communication and Computer Systems (ICCS) 
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 */

/**
 * @file   bitmap/debug.c
 * @author Ioannis Koutras (joko@microlab.ntua.gr)
 * @date   October 2012
 * @brief  Debug functions implementation for bitmap-organised raw blocks
 */

#include "bitmap/debug.h"

#include <inttypes.h>
#include <string.h>

#include "dmmlib_trace.h"

/**
 * Returns the number of used cells (including the ones of the bitmap vector not
 * actually used) in a bitmap-organized raw block.
 */
size_t get_used_cells(bitmap_rb_t *raw_block);

size_t get_used_cells(bitmap_rb_t *raw_block) {
    size_t count, i, j;
    BMAP_EL_TYPE *bmap_p, buffer;

    bmap_p = (BMAP_EL_TYPE *)((uintptr_t) raw_block + sizeof(bitmap_rb_t));
    count = 0;

    for(i = 0; i < raw_block->elements; i++) {
        buffer = *bmap_p;
        for(j = 0; j < BMAP_EL_SIZE_BITS; j++) {
            if((buffer & (BMAP_EL_TYPE) 1) == (BMAP_EL_TYPE) 1) {
                count++;
            }
            buffer >>= 1;
        }
        bmap_p++;
    }
    return raw_block->elements * BMAP_EL_SIZE_BITS - count;
}

void print_bitmap_vector(bitmap_rb_t *raw_block) {
    char output[raw_block->elements * BMAP_EL_SIZE_BITS + 1], *el_out;
    BMAP_EL_TYPE *bmap_p, z;

    bmap_p = (BMAP_EL_TYPE *)((uintptr_t) raw_block + sizeof(bitmap_rb_t));

    output[0] = '\0';

    el_out = &output[0];

    for(size_t i = 0; i < raw_block->elements; i++) {
        for(z = 1; z <= ((BMAP_EL_TYPE) 1 << (BMAP_EL_SIZE_BITS - 1)); z <<= 1) {
            strcat(el_out, ((*bmap_p & z) == z) ? "0" : "1");
            if(z == ((BMAP_EL_TYPE) 1 << (BMAP_EL_SIZE_BITS - 1))) {
                break;
            }
        }
        el_out += BMAP_EL_SIZE_BITS;
        bmap_p++;
    }

    DBG_TRACE("Cells in data layout: %s\n", output);
    DBG_TRACE("dmmlib - mem - bm - %zu\n", get_used_cells(raw_block));

    return;
}
