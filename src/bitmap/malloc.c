/*
 *   Copyright Institute of Communication and Computer Systems (ICCS) 
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 */

/**
 * @file   bitmap/malloc.c
 * @author Ilias Pliotas, Ioannis Koutras
 * @date   September, 2012
 * @brief  malloc() implementation for bitmap-organized raw blocks
 */

#include "bitmap/bitmap.h"
#include "bitmap/bitmap_rb.h"
#include "bitmap/bitmap_other.h"

#include "dmmlib_trace.h"
#include "statistics.h"

/**
 * Returns a memory block from a bitmap-organized raw block
 *
 * @param  raw_block The pointer of the bitmap raw block.
 * @param  req_size  The requested memory size.
 * @retval           The address to serve the request.
 * @retval NULL      No available memory space.
 */
void * bitmap_malloc(bitmap_rb_t *raw_block, size_t req_size) { 
    size_t cells, stop, i, start_pos;
    void *ret;
    chunk_header_t *chunk_address;
    BMAP_EL_TYPE *bmap_p;
    BMAP_EL_TYPE temp1[raw_block->elements], temp2[raw_block->elements];

    ret = NULL;

    req_size += CHUNK_HDR_SIZE;

    if(req_size % raw_block->bytes_per_cell > 0) {
        cells = req_size / raw_block->bytes_per_cell + 1;
    } else {
        cells = req_size / raw_block->bytes_per_cell;
    }

    stop = prev_pow2(cells);

    bmap_p = (BMAP_EL_TYPE *)((char *)raw_block + sizeof(bitmap_rb_t));

    copy_array(temp1, bmap_p, raw_block->elements);

    // perform bitwise shift & and operations in the BMAP_EL_TYPE arrays
    for(i = 1; i < stop; i <<= 1) {
        copy_array(temp2, temp1, raw_block->elements);
        shift_array(temp2, i, raw_block->elements);
        add_arrays(temp1, temp2, raw_block->elements);
    }
    if(stop < cells) {
        copy_array(temp2, temp1, raw_block->elements);
        shift_array(temp2, cells - stop, raw_block->elements);
        add_arrays(temp1, temp2, raw_block->elements);
    }

    start_pos = 0;

    for(i = 0; i < raw_block->elements; ++i) {

        // __builtin_ffsl() returns the position of the first 1 starting from 1
        start_pos = (size_t) __builtin_ffsl((long) temp1[i]);
        // so if an 1 is found, it will change from 0 to the position
        if(start_pos) {

            // 1. Mark the occupied cells

            size_t cell_no = i * BMAP_EL_SIZE_BITS + start_pos - 1;
            size_t mask_counter = cells;

            while(mask_counter != 0) {
                if(mask_counter > BMAP_EL_SIZE_BITS) {
                    bmap_p[i] &= ~make_bit_mask(start_pos, 
                            BMAP_EL_SIZE_BITS - start_pos + 1);
                    mask_counter -= BMAP_EL_SIZE_BITS - start_pos + 1;
                    i++;
                    start_pos = 1;
                } else {
                    bmap_p[i] &= ~make_bit_mask(start_pos, 
                            mask_counter);
                    mask_counter = 0;
                }
            }

            // 2. Calculate the pointer to the chunk to be retrieved

            chunk_address = (chunk_header_t *)((char *)raw_block +
                    sizeof(bitmap_rb_t) +
                    raw_block->elements * BMAP_EL_SIZE +
                    cell_no * raw_block->bytes_per_cell);

            // 3. Update the chunk header

            chunk_address->num_of_cells = cells;
#ifdef REQUEST_SIZE_INFO
            chunk_address->requested_size = req_size - CHUNK_HDR_SIZE;
#endif /* REQUEST_SIZE_INFO */

            // 4. Update the return pointer

            ret = (void *)((char *)chunk_address + CHUNK_HDR_SIZE);

            break;
        }
    }

    return ret;
} 
