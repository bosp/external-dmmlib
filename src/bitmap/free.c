/*
 *   Copyright Institute of Communication and Computer Systems (ICCS) 
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 */

/**
 * @file   bitmap/free.c
 * @author Ilias Pliotas, Ioannis Koutras
 * @date   September 2012
 * @brief  free() implementation for bitmap-organized raw blocks
 */

#include "dmmlib_trace.h"
#include "statistics.h"

#include "bitmap/bitmap.h"
#include "bitmap/bitmap_rb.h"
#include "bitmap/bitmap_other.h"

/** Frees the memory block inside of a specific bitmap-organized raw block.
 * @param raw_block The pointer of the bitmap raw block.
 * @param ptr       The pointer of the memory block to be freed.
 */
void bitmap_free(bitmap_rb_t *raw_block, void *ptr) {
    BMAP_EL_TYPE *bmap_p;
    chunk_header_t *chunk_header;
    size_t cells_used, cell_no; 

    bmap_p = (BMAP_EL_TYPE *)((uintptr_t) raw_block + sizeof(bitmap_rb_t));
    chunk_header = (chunk_header_t *)((char *)ptr - CHUNK_HDR_SIZE);

    cells_used = chunk_header->num_of_cells;

    cell_no = (size_t) ((uintptr_t) chunk_header - (uintptr_t) bmap_p -
            raw_block->elements * BMAP_EL_SIZE)
        / raw_block->bytes_per_cell;

    size_t mask_counter = cells_used;
    size_t mask_start = cell_no % BMAP_EL_SIZE_BITS + 1;
    size_t vector_index = cell_no / BMAP_EL_SIZE_BITS;

    while(mask_counter != 0) {
        if(mask_counter > BMAP_EL_SIZE_BITS) {
            bmap_p[vector_index] |= make_bit_mask(mask_start, 
                    BMAP_EL_SIZE_BITS - mask_start + 1);
            mask_counter -= BMAP_EL_SIZE_BITS - mask_start + 1;
            vector_index++;
            mask_start = 1;
        } else {
            bmap_p[vector_index] |= make_bit_mask(mask_start, 
                    mask_counter);
            mask_counter = 0;
        }
    }

#ifdef REQUEST_SIZE_INFO
    UPDATE_GLOBAL_STATS(FREE, chunk_header->requested_size);
#else /* REQUEST_SIZE_INFO */
    UPDATE_GLOBAL_STATS(FREE);
#endif /* REQUEST_SIZE_INFO */

}
