/*
 *   Copyright Institute of Communication and Computer Systems (ICCS) 
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 */

/**
 * @file   src/other.c
 * @author Ioannis Koutras (joko@microlab.ntua.gr)
 * @date   October 2012
 *
 * @brief  Implementation of auxiliary calls.
 */

#include "other.h"

#include <inttypes.h>

/** Finds the raw block owner of a certain pointer */
raw_block_header_t * find_raw_block_owner(struct rb_head_s head, void* ptr) {
    raw_block_header_t *current_block, *owner;

    owner = NULL;

    SLIST_FOREACH(current_block, &head, pointers) {
        if(((uintptr_t) current_block < (uintptr_t) ptr) &&
                ((uintptr_t) ptr < (uintptr_t) current_block +
                 current_block->size)
          ) {
            owner = current_block;
            break;
        }
    }

    return owner;
}
