/*
 *   Copyright Institute of Communication and Computer Systems (ICCS) 
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 */

/**
 * @file   release_memory.c
 * @author Ioannis Koutras (joko@microlab.ntua.gr)
 * @date   September 2012
 * @brief  Implementation of the function to release memory space back to the
 * system.
 */

#include "release_memory.h"

#include <sys/mman.h>

#include <unistd.h>
#include <inttypes.h>
#include <assert.h>

void release_memory(raw_block_header_t *raw_block) {
    int error_code;

    size_t pagesize = (size_t) sysconf(_SC_PAGESIZE);
    size_t padding = (uintptr_t) raw_block % pagesize;

    if(padding != 0) {
        error_code = munmap((void *) ((uintptr_t) raw_block - padding),
                    raw_block->size + padding);
    } else {
        error_code = munmap((void *)raw_block, raw_block->size);
    }
    assert(error_code == 0);
}
