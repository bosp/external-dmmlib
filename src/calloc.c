/*
 *   Copyright Institute of Communication and Computer Systems (ICCS) 
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 */

/**
 * @file   calloc.c
 * @author Ioannis Koutras (joko@microlab.ntua.gr)
 * @date   September 2012
 *
 * @brief  Implementations of calloc() call.
 */

#include "dmmlib/dmmlib.h"

#include <string.h> /* for memset() */

void * calloc(size_t nmemb, size_t size) {
    void *result;

    if(nmemb == 0 || size == 0) {
        return NULL;
    }

    size_t total_size = nmemb * size;

    result = malloc(total_size);

    if(result != NULL) {
        memset(result, 0, total_size);
    }

    return result;
}
