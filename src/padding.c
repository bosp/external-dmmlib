/*
 *   Copyright Institute of Communication and Computer Systems (ICCS) 
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 */

/**
 * @file   src/padding.c
 * @author Ioannis Koutras (joko@microlab.ntua.gr)
 * @date   March 2013
 *
 * @brief  Implementation of padding for alignment.
 */

#include "padding.h"

#include "dmmlib/config.h"

/** Tries to align the input to 32, 64, 128, 256 or to a multiple of processor
 * word width if it is larger.
 *
 * @param size The input number.
 */
size_t req_padding(size_t size) {
    size_t align;

    if(size <= 32)
        return 32;
    if(size <= 64)
        return 64;
    if(size <= 128)
        return 128;
    if(size <= 256)
        return 256;

    align = size % DMM_DATA_ALIGNMENT;
    if(align != 0) {
        size += (DMM_DATA_ALIGNMENT - align);
    }

    return size;
}


