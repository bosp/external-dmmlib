/*
 *   Copyright 2011 Institute of Communication and Computer Systems (ICCS) 
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 */

#include <dmmlib/print_stats.h>
#include <dmmlib/config.h>

#include <stdio.h>

size_t get_allocated_space(heap_t *heap) {
    return heap->dmm_stats.mem_allocated;
}

size_t get_used_space(heap_t *heap) {
    return heap->dmm_stats.mem_used;
}

#ifdef REQUEST_SIZE_INFO
size_t get_requested_space(heap_t *heap) {
    return heap->dmm_stats.mem_requested;
}
#endif /* REQUEST_SIZE_INFO */

void print_stats(allocator_t *allocator) {
    int i;

    for(i = 0; i < NUM_HEAPS; i++) {
        printf("dmmlib - Heap %d statistics:\n", i);
        printf("dmmlib - Total malloc() calls: %d\n",
                (int) allocator->heaps[i].dmm_stats.num_malloc);
        printf("dmmlib - Total free() calls: %d\n",
                (int) allocator->heaps[i].dmm_stats.num_free);
#ifdef REQUEST_SIZE_INFO
        printf("dmmlib - Memory currently requested: %zu\n",
                get_requested_space(&allocator->heaps[i]));
#endif /* REQUEST_SIZE_INFO */
        printf("dmmlib - Memory currently used: %zu\n",
                get_used_space(&allocator->heaps[i]));
        printf("dmmlib - Memory currently allocated: %zu\n",
                get_allocated_space(&allocator->heaps[i]));
    }
}
