/*
 *   Copyright Institute of Communication and Computer Systems (ICCS) 
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 */

/**
 * @file   src/debug.c
 * @author Ioannis Koutras (joko@microlab.ntua.gr)
 * @date   September 2012
 * @brief  Debug functions implementation
 */

#include "dmmlib/debug.h"

#include "dmmlib_trace.h"
#include <stdbool.h>
#include "dmmlib/dmmlib.h"
#include "locks.h"

#include "dmmlib/freelist/freelist_rb.h"
#include "freelist/debug.h"

/** Gets the number of raw blocks that are currently managed by an allocator.
 *
 * @param allocator The pointer of the allocator's data structure.
 */
void get_raw_blocks(allocator_t *allocator) {
    int counter;
    raw_block_header_t *current_raw_block;

    counter = 0;

    SLIST_FOREACH(current_raw_block, &allocator->rb_head, pointers) {
        counter++;
        DBG_TRACE("dmmlib - Raw block at %p of size %zu\n",
                (void *)current_raw_block,
                current_raw_block->size);
        get_memory_blocks((freelist_rb_t *)((uintptr_t) current_raw_block +
                    sizeof(raw_block_header_t)));
    }

    DBG_TRACE("dmmlib - there are %d raw blocks\n", counter);

    counter = 0;

    SLIST_FOREACH(current_raw_block, &allocator->bb_head, pointers) {
        counter++;
        DBG_TRACE("dmmlib - Raw block at %p of size %zu\n",
                (void *)current_raw_block,
                current_raw_block->size);
    }

    DBG_TRACE("dmmlib - there are %d big blocks\n", counter);
}
