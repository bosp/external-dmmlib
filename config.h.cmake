/*
 *   Copyright Institute of Communication and Computer Systems (ICCS)
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 */

/**
 * @file   dmmlib/config.h
 * @author Ioannis Koutras (joko@microlab.ntua.gr)
 * @date   December 2012
 * @brief  Macro defines for dmmlib.
 *
 * Warning: This is an automatically-generated file, you have to configure,
 * compile and install dmmlib again in order to change the following macros.
 */

#ifndef DMM_CONFIG_H
#define DMM_CONFIG_H

/* Version Numbers */

/**
 * @brief The main version of dmmlib
 *
 * This version is checked at library initialization time. A major version match
 * is required in order to properly setup the runtime communication, otherwise
 * the library initilization shall fail.
 */
#define DMMLIB_VERSION_MAJOR @APPLICATION_VERSION_MAJOR@
/**
 * @brief The revision version of dmmlib
 *
 * The minor version is increased at each internal library update which does not
 * cause any backward incompatibility.
 */
#define DMMLIB_VERSION_MINOR @APPLICATION_VERSION_MINOR@

/* General Settings */

/** Support for freelist-organized raw blocks */
#cmakedefine FL_RB_ONLY
/** Support for bitmap-organized raw blocks */
#cmakedefine BITMAP_RB_ONLY
/** Default raw block size */
#cmakedefine SYS_ALLOC_SIZE @SYS_ALLOC_SIZE@
/** Support for locks */
#cmakedefine HAVE_LOCKS

/* Free-list Settings */
#ifdef FL_RB_ONLY

/* Fitting Policies */

/** Support for best-fit policy */
#cmakedefine BEST_FIT
/** Support for exact-fit policy */
#cmakedefine EXACT_FIT
/** Support for first-fit policy */
#cmakedefine FIRST_FIT
/** Support for good-fit policy */
#cmakedefine GOOD_FIT
/** Good-fit percentage */
#cmakedefine GOOD_FIT_PERCENTAGE @GOOD_FIT_PERCENTAGE@

/* Ordering Policies */

/** Support for address-ordering */
#cmakedefine ADDRESS_ORDERED
/** Support for FIFO-ordering */
#cmakedefine FIFO_ORDERED
/** Support for LIFO-ordering */
#cmakedefine LIFO_ORDERED
/** Support for size-ordering */
#cmakedefine SIZE_ORDERED

/* Coalescing Settings */

/** Support of coalescing of fixed size */
#cmakedefine COALESCING_FIXED
/** Support of coalescing of variable size */
#cmakedefine COALESCING_VARIABLE

/** Default maximum coalescing size */
#cmakedefine MAX_COAL_SIZE @MAX_COAL_SIZE@

/* Splitting Settings */

/** Support of splitting of fixed size */
#cmakedefine SPLITTING_FIXED
/** Support of splitting of variable size */
#cmakedefine SPLITTING_VARIABLE

/** Default minimum splitting size */
#cmakedefine MIN_SPLIT_SIZE @MIN_SPLIT_SIZE@

/** Support to coalesce after splitting */
#cmakedefine COALESCE_AFTER_SPLIT

#endif /* FL_RB_ONLY */

/* Bitmap Settings */
#ifdef BITMAP_RB_ONLY

/** How many bytes per cell should be used */
#cmakedefine BITMAP_RESOLUTION @BITMAP_RESOLUTION@

#endif /* BITMAP_RB_ONLY */

/** Keep track of the requested size */
#cmakedefine REQUEST_SIZE_INFO

/** Support for statistics per raw block */
#cmakedefine WITH_RAWBLOCK_STATS
/** Support for global statistics */
#cmakedefine WITH_ALLOCATOR_STATS

/** Support for knobs */
#cmakedefine WITH_KNOBS

/** Support for debugging functions */
#cmakedefine WITH_DEBUG

/** Support for memory trace */
#cmakedefine WITH_MEM_TRACE
/** Support for statistics trace */
#cmakedefine WITH_STATS_TRACE

/** Support for realloc() calls */
#cmakedefine WITH_REALLOC

/** Support to parse dmmlib environment variables */
#cmakedefine PARSE_ENV

/** Support for memalign() calls */
#cmakedefine WITH_MEMALIGN

/** Make raw block requests for sizes that are multiple of the pagesize */
#cmakedefine PAGESIZE_ALIGN

/** Data alignment */
#cmakedefine DMM_DATA_ALIGNMENT @DMM_DATA_ALIGNMENT@

#endif /* DMM_CONFIG_H */

