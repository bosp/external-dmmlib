ifdef CONFIG_EXTERNAL_DMMLIB

# Targets provided by this project
.PHONY: dmmlib clean_dmmlib

# Add this to the "external" target
external: dmmlib
clean_external: clean_dmmlib

dmmlib: setup $(BUILD_DIR)/lib/libdmm.so
$(BUILD_DIR)/lib/libdmm.so:
	@echo
	@echo "==== Building DMM Library ($(BUILD_TYPE)) ===="
	@[ -d external/dmmlib/build/$(BUILD_TYPE) ] || \
		mkdir -p external/dmmlib/build/$(BUILD_TYPE) || \
		exit 1
	@cd external/dmmlib/build/$(BUILD_TYPE) && \
		sed -n -e 's/^CONFIG_DMMLIB_\([^=]*\)=y/set(\1 ON)/ p' \
		-e 's/^CONFIG_DMMLIB_\([^=]*\)=\([^y]*\).*/set(\1 \2)/ p' \
		<$(KCONFIG_CONFIG) >bosp-config.cmake && \
		CC=$(CC) CFLAGS="$(CFLAGS_SYSROOT) -fPIC" \
		cmake -DCMAKE_INSTALL_PREFIX=$(BUILD_DIR) ../.. || \
		exit 1
	@cd external/dmmlib/build/$(BUILD_TYPE) && \
		make -j$(CPUS) install || \
		exit 1

clean_dmmlib:
	@echo
	@echo "==== Clean-up DMM Library ($(BUILD_TYPE)) ===="
	@[ ! -f $(BUILD_DIR)/lib/libdmm.so ] || \
		rm -f  $(BUILD_DIR)/lib/libdmm* && \
		rm -rf $(BUILD_DIR)/include/dmmlib
	@rm -rf external/dmmlib/build
	@echo

else # CONFIG_EXTERNAL_DMMLIB

dmmlib:
	$(warning external/dmmlib module disabled by BOSP configuration)
	$(error BOSP compilation failed)

endif # CONFIG_EXTERNAL_DMMLIB
