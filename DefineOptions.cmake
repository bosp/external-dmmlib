# Main Settings

set(WITH_SYSTEM_CALLS "none" CACHE STRING "Choose what system calls can be used, options are: none, sbrk, mmap")
set(SYS_ALLOC_SIZE 4096 CACHE INTEGER "Choose the default system allocation size")

option(HAVE_LOCKS "Build with POSIX locking mechanisms" OFF)

set(RAW_BLOCKS_TYPE "freelist" CACHE STRING "Choose what raw blocks can be used, options are: freelist, bitmap")

# Free-list Settings

set(ORDERING_POLICY "lifo" CACHE STRING "Choose the ordering policy in freelist-organized raw blocks, options are: address, fifo, lifo, size")

set(FITTING_POLICY "best" CACHE STRING "Choose the fitting policy in freelist-organized raw blocks, options are: best, exact, first, good")

set(GOOD_FIT_PERCENTAGE 0.8 CACHE DOUBLE "Choose the good-fit percentage")

set(WITH_COALESCING "never" CACHE STRING "Build with coalescing support")
set(WITH_SPLITTING "never" CACHE STRING "Build with splitting support")
option(FREELIST_COALESCE_AFTER_SPLIT "Try to coalesce blocks after split" OFF)

# Bitmap Settings

set(BITMAP_RESOLUTION 256 CACHE INTEGER "Choose the size of cells in bitmap-organised raw blocks")

# Function Implementation Settings

option(WITH_REALLOC "Build with realloc() support" ON)
option(WITH_CALLOC "Build with calloc() support" ON)
option(WITH_MEMALIGN "Build with memalign() support" ON)

# Knobs Settings

option(WITH_KNOBS "Build with knobs support" OFF)

# Statistics Settings

set(STATS "none" CACHE STRING "Choose if the memory allocator keeps internally statistics per raw block or globally, options are: none, global")
set(REQUEST_SIZE_INFO OFF)

# Trace Settings

option(WITH_MEM_TRACE "Support for memory traces" OFF)
option(WITH_STATS_TRACE "Support for statistics traces" OFF)

# Build Settings

option(WITH_STATIC_LIB "Build a static library" OFF)
option(WITH_SHARED_LIB "Build a shared library" OFF)

# Misc Settings

option(PARSE_ENV "Build with support to parse dmmlib environment variables" OFF)
option(WITH_EXAMPLES "Build with examples" OFF)
option(WITH_DOC "Build with documentation" OFF)

# Presets

option(P2012 "Build for P2012 runtime" OFF)
option(LEON3 "Build for Leon3" OFF)
option(LINUX "Build for Linux" OFF) 

if(P2012)
  if(CMAKE_CROSSCOMPILING)
    set(CMAKE_C_ARCHIVE_CREATE "<CMAKE_AR> -mcore=stxp70v4 <LINK_FLAGS> cr <TARGET> <OBJECTS>")
    set(CMAKE_C_ARCHIVE_FINISH "<CMAKE_RANLIB> -mcore=stxp70v4 <TARGET>")
  endif(CMAKE_CROSSCOMPILING)
  set(DMM_DATA_ALIGNMENT "4") # Do not change
  set(WITH_SYSTEM_CALLS "none") # Do not change
  set(HAVE_LOCKS OFF) # Do not change
  set(RAW_BLOCKS_TYPE "freelist")
  set(ORDERING_POLICY "lifo")
  set(FITTING_POLICY "first")
  set(WITH_COALESCING "never")
  set(WITH_SPLITTING "never")
  set(WITH_REALLOC OFF)
  set(WITH_CALLOC OFF)
  set(WITH_MEMALIGN OFF)
  set(WITH_KNOBS OFF)
  set(STATS "none")
  set(REQUEST_SIZE_INFO OFF)
  set(WITH_MEM_TRACE OFF)
  set(WITH_STATS_TRACE OFF)
  set(WITH_STATIC_LIB ON)
  set(WITH_SHARED_LIB OFF)
  set(PARSE_ENV OFF)
  set(WITH_EXAMPLES OFF)
  set(WITH_DOC OFF)
endif(P2012)

if(LEON3)
  set(WITH_SYSTEM_CALLS "none")
  set(HAVE_LOCKS OFF)
  set(RAW_BLOCKS_TYPE "freelist")
  set(ORDERING_POLICY "lifo")
  set(FITTING_POLICY "first")
  set(WITH_COALESCING "never")
  set(WITH_SPLITTING "never")
  set(WITH_REALLOC OFF)
  set(WITH_CALLOC OFF)
  set(WITH_MEMALIGN OFF)
  set(WITH_KNOBS OFF)
  set(STATS "none")
  set(REQUEST_SIZE_INFO OFF)
  set(WITH_MEM_TRACE OFF)
  set(WITH_STATS_TRACE OFF)
  set(WITH_STATIC_LIB ON)
  set(WITH_SHARED_LIB OFF)
  set(PARSE_ENV OFF)
  set(WITH_EXAMPLES OFF)
  set(WITH_DOC OFF)
endif(LEON3)

if(LINUX)
  set(WITH_SYSTEM_CALLS "mmap")
  set(SYS_ALLOC_SIZE 32768)
  set(HAVE_LOCKS ON)
  set(RAW_BLOCKS_TYPE "freelist")
  set(ORDERING_POLICY "lifo")
  set(FITTING_POLICY "good")
  set(GOOD_FIT_PERCENTAGE 0.8)
  set(WITH_COALESCING "variable")
  set(MAX_COAL_SIZE 60000)
  set(WITH_SPLITTING "variable")
  set(MIN_SPLIT_SIZE 64)
  set(FREELIST_COALESCE_AFTER_SPLIT ON)
  set(WITH_REALLOC ON)
  set(WITH_CALLOC ON)
  set(WITH_MEMALIGN ON)
  set(WITH_KNOBS ON)
  set(STATS "global")
  set(REQUEST_SIZE_INFO ON)
  set(WITH_MEM_TRACE ON)
  set(WITH_STATS_TRACE ON)
  set(WITH_SHARED_LIB ON)
  set(WITH_STATIC_LIB ON)
  set(PARSE_ENV ON)
  set(WITH_EXAMPLES ON)
  set(WITH_DOC ON)
endif(LINUX)
