/*
 *   Copyright 2012 Institute of Communication and Computer Systems (ICCS) 
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 */

/**
 * @file   freelist_rb.h
 * @author Ioannis Koutras (joko@microlab.ntua.gr)
 * @date   August 2012
 * @brief  Freelist raw block structure and creation function.
 */

#ifndef FREELIST_RB_H
#define FREELIST_RB_H
#include "dmmlib/freelist/block_header.h"
#include "dmmlib/lists.h"

/** Data structure of the required elements for a free-list organized raw block.
 */
typedef struct freelist_rb_s {
    size_t remaining_size; /**< The remaining size for new memory blocks. */
    block_header_t *border_ptr; /**< Pointer to the memory block allocated last. */
    struct fl_head_s fl_head; /**< Head of the free list of memory blocks. */
#ifdef FIFO_ORDERED
    block_header_t *fl_tail; /**< The tail of the free list. */
#endif /* FIFO_ORDERED */
} freelist_rb_t;

#endif /* FREELIST_RB_H */
