/*
 *   Copyright 2011 Institute of Communication and Computer Systems (ICCS) 
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 */

/**
 * \file 	block_header.h
 * \author 	Ioannis Koutras (joko@microlab.ntua.gr)
 * \date 	September, 2011
 *  
 * \brief Block header structure and functions, and memory block functions.
 */

#ifndef BLOCK_HEADER_H
#define BLOCK_HEADER_H

#include <stddef.h>
#include <stdbool.h>

#include <dmmlib/config.h>
#include "dmmlib/lists.h"

/** The header structure of every memory block inside a heap. */
typedef struct block_header_s {
    size_t size; /**< The LSB represents the availability of the block (1 for
                   used, 0 for free), the rest the size of the data part. */
#ifdef REQUEST_SIZE_INFO
    size_t requested_size; /**< The requested size of the data part */
#endif /* REQUEST_SIZE_INFO */
    size_t previous_size; /**< The LSB represents the availability of the
                            previous block, the rest the size of the data
                            part of the previous block in the memory space */
    /** Pointer to the next memory block in the list */
    SLIST_ENTRY(block_header_s) pointers;
} block_header_t;

/** The head element of a singly-linked list of block_header_t records */
SLIST_HEAD(fl_head_s, block_header_s);

/**
 * The size of the header in number of bytes
 */
#define HEADER_SIZE sizeof(block_header_t)

#endif /* BLOCK_HEADER_H */
