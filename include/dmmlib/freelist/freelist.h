/*
 *   Copyright Institute of Communication and Computer Systems (ICCS) 
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 */

/**
 * \file    freelist.h
 * \author  Ioannis Koutras (joko@microlab.ntua.gr)
 * \brief   Memory allocation and de-allocation for free-list based raw blocks
 */

#ifndef FREELIST_H
#define FREELIST_H
#include <dmmlib/config.h>

#include "dmmlib/freelist/freelist_rb.h"

#ifdef FL_RB_ONLY

/** Tries to allocate memory from a specific free-list organized raw block.
 * @param raw_block The pointer of the freelist-organised raw block.
 * @param size      The requested size.
 * @retval          The address of the returned memory space.
 */
#define dmmlib_malloc(raw_block, size) freelist_malloc(raw_block, size)

/** Frees the memory block inside of a specific free-list organized raw block.
 * @param raw_block The pointer of the freelist raw block.
 * @param ptr       The pointer of the memory block to be freed.
 */
#define dmmlib_free(raw_block, ptr) freelist_free(raw_block, ptr)

/**
 * Reallocates a memory block from a freelist-organized raw block
 *
 * @param  raw_block The pointer of the freelist raw block.
 * @param  ptr       The pointer of the memory block to be re-allocated.
 * @param  size      The requested memory size.
 * @retval           The address to serve the request.
 * @retval NULL      No available memory space.
 */
#define dmmlib_realloc(raw_block, ptr, size) freelist_realloc(raw_block, ptr, size)

#endif /* FL_RB_ONLY */

void * freelist_malloc(freelist_rb_t *raw_block, size_t size);

void freelist_free(freelist_rb_t *raw_block, void *ptr);

void * freelist_realloc(freelist_rb_t *raw_block, void *ptr, size_t size);

#ifdef WITH_MEMALIGN
/**
 * Tries to allocate memory in an aligned address from a freelist-organized raw
 * block.
 *
 * @param  raw_block The pointer of the freelist raw block.
 * @param  alignment The alignment that you want to use for the memory.
 * @param  size      The requested memory size, in bytes.
 * @retval           The address to serve the request.
 * @retval NULL      No available memory space.
 */
void * freelist_memalign(freelist_rb_t *raw_block, size_t alignment,
        size_t size);
#endif /* WITH_MEMALIGN */

#endif /* FREELIST_H */
