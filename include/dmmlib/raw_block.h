/*
 *   Copyright Institute of Communication and Computer Systems (ICCS)
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 */

/**
 * @file   raw_block.h
 * @author Ioannis Koutras (joko@microlab.ntua.gr)
 * @date   September 2012
 * @brief  Raw block structure and functions.
 */

#ifndef RAW_BLOCK_H
#define RAW_BLOCK_H
#include <dmmlib/config.h>

#include <stddef.h>

#ifdef HAVE_LOCKS
#include <pthread.h> /* FIXME To be removed once mutex is removed. */
#endif /* HAVE_LOCKS */

#include <dmmlib/lists.h>

#ifdef WITH_RAWBLOCK_STATS
#include <dmmlib/dmmstats.h>
#endif /* WITH_RAWBLOCK_STATS */

/** Enumeration of raw block's different types */
typedef enum rb_type_en {
#ifdef FL_RB_ONLY
    FREELIST,
#endif /* FL_RB_ONLY */
#ifdef BITMAP_RB_ONLY
    BITMAP,
#endif /* BITMAP_RB_ONLY */
    BIGBLOCK
} rb_type;

/** The header structure of every raw block inside a heap. */
typedef struct raw_block_header_s {
    rb_type type; /**< The type of the raw block. */
    size_t size; /**< Total size of the raw block. */
#ifdef REQUEST_SIZE_INFO
    size_t requested_size; /**< The requested size of the raw block. */
#endif /* REQUEST_SIZE_INFO */
    SLIST_ENTRY(raw_block_header_s) pointers; /**< Pointer to the next raw
                                                block. */
#ifdef HAVE_LOCKS
    pthread_mutex_t mutex;/**< Mutex. */
#endif /* HAVE_LOCKS */
} raw_block_header_t;

/** The head element of a singly-linked list of raw_block_header_t records */
SLIST_HEAD(rb_head_s, raw_block_header_s);

/** Create a new raw block. */
raw_block_header_t *create_raw_block(size_t raw_block_size, rb_type type);

#endif /* RAW_BLOCK_H */
