/*
 *   Copyright 2012 Institute of Communication and Computer Systems (ICCS) 
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 */

/**
 * @file   dmmlib.h
 * @author Ioannis Koutras (joko@microlab.ntua.gr)
 * @date   August 2012
 * @brief  Memory allocation and de-allocation functions.
 *
 * This file should be used when linking directly dmmlib to your project.
 * Warning: In case you also use the C standard library, do not forget to link
 * dmmlib first or else your project will use C standard library's
 * implementations for dynamic memory management.
 */

#ifndef DMMLIB_H
#define DMMLIB_H
#include <dmmlib/config.h>
#include <dmmlib/allocator.h>

/** Global variable storing allocator's settings */
allocator_t systemallocator;

#if !defined _STDLIB_H && !defined _STDLIB_H_

/**
 * Allocates size bytes by using the system's allocator.
 * @param size Requested allocation size in bytes.
 *
 * @return The pointer to the memory location.
 */
void * malloc(size_t size);

/**
 * De-allocates the memory space pointed to by ptr.
 * @param   ptr     The pointer to memory to free.
 */
void free(void *ptr);

/**
 * Try to change the size of an allocation
 * @param   ptr	    The pointer to the data part of the original memory block.
 * @param   size    The new desired size of the block.
 *
 * @return  The pointer to the new memory location.
 */
void * realloc(void *ptr, size_t size);

/**
 * Allocate enough space for a number of objects of a specific size.
 * @param   count   The number of objects.
 * @param   size    The size of one object.
 *
 * @return  A pointer to the allocated memory.
 */
void * calloc(size_t count, size_t size);

#ifdef WITH_MEMALIGN
#include <dmmlib/memalign.h>
#endif /* WITH_MEMALIGN */

#endif /* ! STDLIB_H */

#endif /* DMMLIB_H */
