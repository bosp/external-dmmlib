/*
 *   Copyright Institute of Communication and Computer Systems (ICCS) 
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 */

/**
 * @file   dmmlib/debug.h
 * @author Ioannis Koutras (joko@microlab.ntua.gr)
 * @date   September 2012
 * @brief  Debug functions
 */

#ifndef DMMLIB_DEBUG_H_
#define DMMLIB_DEBUG_H_
#include <dmmlib/config.h>

#ifdef WITH_DEBUG

#include "dmmlib/allocator.h"
#include "dmmlib/raw_block.h"

void get_raw_blocks(allocator_t *allocator);

#endif /* WITH_DEBUG */

#endif /* DMMLIB_DEBUG_H_ */
