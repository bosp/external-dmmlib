/*
 *   Copyright Institute of Communication and Computer Systems (ICCS) 
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 */

/**
 * @file   knobs.h
 * @author Ioannis Koutras (joko@microlab.ntua.gr)
 * @date   November 2012
 * @brief  Basic knob structure
 */

#ifndef KNOBS_H
#define KNOBS_H

#include <dmmlib/config.h>

#include <dmmlib/dmmstats.h>

/** The knobs structure of dmmlib. */
typedef struct dmm_knobs_s {

    /** Set of configuration parameters */
    struct p {
        double sys_alloc_size; /**< The default size of the raw block. */
#ifdef GOOD_FIT
        double fit_percentage; /**< The good-fit percentage. */
#endif /* GOOD_FIT */
#ifdef COALESCING_VARIABLE
        double max_coal_size; /**< Maximum allowed size to be coalesced. */
#endif /* COALESCING_VARIABLE */
#ifdef SPLITTING_VARIABLE
        double min_split_size; /**< Minimum allowed size to be split. */
#endif /* SPLITTING_VARIABLE */
    } p;

    /** Set of profiled metrics */
    struct m {
#ifdef REQUEST_SIZE_INFO
        double tot_req_size; /**< Total requested size */
#endif /* WITH_REQUEST_SIZE_INFO */
        double tot_allocated_size; /**< Total allocated size */
    } m;

} dmm_knobs_t;

/** Sets the system allocator's knob structure. */
uint32_t dmm_set_knobs(dmm_knobs_t *conf);

/** Notifies dmmlib that an application cycle has been completed. */
void dmm_notify_cycle(void);

#endif /* KNOBS_H */
