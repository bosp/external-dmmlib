/*
 *   Copyright Institute of Communication and Computer Systems (ICCS)
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 */

/**
 * @file   allocator.h
 * @author Ioannis Koutras (joko@microlab.ntua.gr)
 * @date   August 2012
 * @brief  Basic structures needed for the dmmlib allocator.
 */

#ifndef ALLOCATOR_H
#define ALLOCATOR_H
#include <dmmlib/config.h>
#include "dmmlib/raw_block.h"

#ifdef HAVE_LOCKS
#include <pthread.h>
#endif /* HAVE_LOCKS */

#ifdef WITH_KNOBS
#include "dmmlib/knobs.h"
#endif /* WITH_KNOBS */

#ifdef WITH_ALLOCATOR_STATS
#include <dmmlib/dmmstats.h>
#endif /* WITH_ALLOCATOR_STATS */

#ifdef PARSE_ENV
#if defined  WITH_MEM_TRACE || defined WITH_STATS_TRACE || defined WITH_DEBUG
#include <stdbool.h>
#endif /* WITH_MEM_TRACE || WITH_STATS_TRACE || WITH_DEBUG */
#endif /* PARSE_ENV */

/** The allocator structure of dmmlib. */
typedef struct allocator_s {
    /** The head of the raw blocks list. */
    struct rb_head_s rb_head;
#ifdef WITH_DEBUG
/** The head of the big blocks list. */
    struct rb_head_s bb_head; 
#endif /* WITH_DEBUG */
#ifdef HAVE_LOCKS
/** Mutex to allow the creation of new raw blocks. */
    pthread_mutex_t creation_mutex;
#endif /* HAVE_LOCKS */
#ifdef WITH_KNOBS
    dmm_knobs_t dmm_knobs; /**< Global knobs. */
#endif /* WITH_KNOBS */
#ifdef WITH_ALLOCATOR_STATS
    dmmstats_t dmm_stats; /**< Global statistics. */
#endif /* WITH_ALLOCATOR_STATS */

#ifdef PARSE_ENV
#if defined  WITH_MEM_TRACE || defined WITH_STATS_TRACE || defined WITH_DEBUG
    /* During initialization it is possible that some files are being opened to
     * write the traces, so some dynamic memory is requested. For such cases the
     * trace functions should not be run. */
    bool initialized; /**< Initialization value. */
#endif /* WITH_MEM_TRACE || WITH_STATS_TRACE || WITH_DEBUG */
#endif /* PARSE_ENV */

} allocator_t;

#endif /* ALLOCATOR_H */
