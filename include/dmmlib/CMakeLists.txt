project(dmmlib-headers C)

set(dmmlib_HDRS
  allocator.h
  dmmlib.h
  dmmstats.h 
  raw_block.h
  lists.h
)

if(WITH_DEBUG)
  set(dmmlib_HDRS ${dmmlib_HDRS} debug.h)
endif(WITH_DEBUG)

if(WITH_KNOBS)
  set(dmmlib_HDRS ${dmmlib_HDRS} knobs.h)
endif(WITH_KNOBS)

if(RAW_BLOCKS_TYPE STREQUAL "freelist")
  set(dmmlib_HDRS
    ${dmmlib_HDRS}
    freelist/initialize.h
    freelist/freelist.h
    freelist/freelist_rb.h
    )
endif(RAW_BLOCKS_TYPE STREQUAL "freelist")

install(
  FILES
    ${dmmlib_HDRS}
  DESTINATION
    ${INCLUDE_INSTALL_DIR}/${APPLICATION_NAME}
  COMPONENT
    headers
)
