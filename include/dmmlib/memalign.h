#ifndef MEMALIGN_H
#define MEMALIGN_H

#include <stddef.h>

int posix_memalign(void **memptr, size_t alignment, size_t size);

#endif /* MEMALIGN_H */
