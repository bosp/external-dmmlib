include(CMakeForceCompiler)

SET(CMAKE_SYSTEM_NAME STxP70v4)

# The environment variables should be already set
# in order to do proper cross compiling
CMAKE_FORCE_C_COMPILER(stxp70cc GNU)
CMAKE_FORCE_CXX_COMPILER(stxp70c++ GNU)

set (CMAKE_C_FLAGS $ENV{CFLAGS}) 
set (CMAKE_SHARED_LIBRARY_LINK_C_FLAGS $ENV{LDFLAGS})

set (CMAKE_AR stxp70-ar)
set (CMAKE_RANLIB stxp70-ranlib)

# search for programs in the build host directories
SET(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
# for libraries and headers in the target directories
SET(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY NEVER)
SET(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE NEVER)
