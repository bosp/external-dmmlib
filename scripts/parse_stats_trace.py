#!/usr/bin/env python

import argparse
import numpy as np

def read_stats_trace(stats_text_file):
    allocated_regexp = r"dmmlib - ms all (\d+)"
    allocated = np.fromregex(stats_text_file.name, allocated_regexp,
            [('num', np.int64)])
    requested_regexp = r"dmmlib - ms req (\d+)"
    requested = np.fromregex(stats_text_file.name, requested_regexp,
            [('num', np.int64)])
    if allocated.size != requested.size:
        print "Warning: Allocated and requested arrays have different sizes"
    return (allocated['num'], requested['num'])

if __name__=='__main__':
    parser = argparse.ArgumentParser(
            description="parse dmmlib's memory traces")
    parser.add_argument("memory_trace", type=argparse.FileType('r'),
                    help="dmmlib memory trace file")
    args = parser.parse_args()
    (allocated, requested) = read_stats_trace(args.memory_trace)
    np.savez_compressed(args.memory_trace.name,
            allocated=allocated, requested=requested)
    args.memory_trace.close()
