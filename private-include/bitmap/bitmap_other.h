/*
 *   Copyright 2012 Institute of Communication and Computer Systems (ICCS) 
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 */

/**
 * @file   bitmap_other.h
 * @author Ilias Pliotas, Ioannis Koutras
 * @date   September 2012
 * @brief  Helping functions for bitmap-organized raw blocks
 */

#ifndef BITMAP_OTHER_H
#define BITMAP_OTHER_H
#include <dmmlib/config.h>

#include "bitmap/bitmap_rb.h"

void copy_array(BMAP_EL_TYPE *dest, BMAP_EL_TYPE *src, size_t elements);

void shift_array(BMAP_EL_TYPE *array, size_t n, size_t elements);

void add_arrays(BMAP_EL_TYPE *array1, BMAP_EL_TYPE *array2, size_t elements);

size_t prev_pow2(size_t n);

BMAP_EL_TYPE make_bit_mask(size_t start_pos, size_t length);

#endif /* BITMAP_OTHER_H */
