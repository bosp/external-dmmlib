/*
 *   Copyright Institute of Communication and Computer Systems (ICCS) 
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 */

/**
 * @file   bitmap_rb.h
 * @author Ioannis Koutras (joko@microlab.ntua.gr)
 * @date   September 2012
 * @brief  Bitmap-organized raw block structure and creation function.
 */

#ifndef BITMAP_RB_H
#define BITMAP_RB_H
#include <dmmlib/config.h>

#include <inttypes.h>
#include <limits.h>
#include <stddef.h> /* for size_t */

/** The data type of the bitmap array element */
#define BMAP_EL_TYPE uint64_t
/** The size of bitmap array element */
#define BMAP_EL_SIZE sizeof(BMAP_EL_TYPE)
/** The size of bitmap array element in bits */
#define BMAP_EL_SIZE_BITS (BMAP_EL_SIZE * CHAR_BIT)
/** Bitmap's initial value */
#define BMAP_EL_INIT_VAL ~((BMAP_EL_TYPE) 0)

/** Bitmap-organized raw block header data structure */
typedef struct bitmap_rb_s {
    size_t bytes_per_cell; /**< The raw block's resolution */
    size_t elements; /**< The necessary amount of bitmap elements */
} bitmap_rb_t;

/** Chunk header data structure */
typedef struct chunk_header_s {
    size_t num_of_cells; /**< The number of occupied cells. */
#ifdef REQUEST_SIZE_INFO
    size_t requested_size; /**< The requested size. */
#endif /* REQUEST_SIZE_INFO */
} chunk_header_t;

/** The size of the chunk header in bitmap-organised raw blocks */
#define CHUNK_HDR_SIZE sizeof(chunk_header_t)

#endif /* BITMAP_RB_H */
