/*
 *   Copyright Institute of Communication and Computer Systems (ICCS) 
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 */

/**
 * @file   bitmap.h
 * @author Ioannis Koutras (joko@microlab.ntua.gr)
 * @brief  Memory allocation for bitmap-organized raw blocks
 */

#ifndef BITMAP_H
#define BITMAP_H
#include <dmmlib/config.h>

#include "bitmap/bitmap_rb.h"

#ifdef BITMAP_RB_ONLY

/** Tries to allocate memory from a specific bitmap-organised raw block.
 * @param raw_block The pointer of the bitmap-organised raw block.
 * @param size      The requested size.
 * @retval          The address of the returned memory space.
 */
#define dmmlib_malloc(raw_block, size) bitmap_malloc(raw_block, size)

/** Frees the memory block inside of a specific bitmap-organised raw block.
 * @param raw_block The pointer of the bitmap-organised raw block.
 * @param ptr       The pointer of the memory block to be freed.
 */
#define dmmlib_free(raw_block, ptr) bitmap_free(raw_block, ptr)

/**
 * Reallocates a memory block from a bitmap-organised raw block
 *
 * @param  raw_block The pointer of the bitmap-organised raw block.
 * @param  ptr       The pointer of the memory block to be re-allocated.
 * @param  size  The requested memory size.
 * @retval           The address to serve the request.
 * @retval NULL      No available memory space.
 */
#define dmmlib_realloc(raw_block, ptr, size) bitmap_realloc(raw_block, ptr, size)

#endif /* BITMAP_RB_ONLY */

void * bitmap_malloc(bitmap_rb_t *raw_block, size_t size);

void bitmap_free(bitmap_rb_t *raw_block, void *ptr);

void * bitmap_realloc(bitmap_rb_t *raw_block, void * ptr,
        size_t req_size);

#endif /* BITMAP_H */
