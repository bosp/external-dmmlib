/*
 *   Copyright Institute of Communication and Computer Systems (ICCS) 
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 */

/**
 * @file   locks.h
 * @author Ioannis Koutras (joko@microlab.ntua.gr)
 * @date   September 2012
 * @brief  Locking functions
 */

#include <dmmlib/config.h>

#ifndef LOCKS_H
#define LOCKS_H

#ifdef HAVE_LOCKS

#include <pthread.h>

/** Locks the systemallocator object */
#define LOCK_GLOBAL() pthread_mutex_lock(&systemallocator.creation_mutex)
/** Unlocks the systemallocator object */
#define UNLOCK_GLOBAL() pthread_mutex_unlock(&systemallocator.creation_mutex)
/** Locks a specific raw block */
#define LOCK_RAW_BLOCK(rb) pthread_mutex_lock(&rb->mutex)
/** Initialized a raw block lock */
#define INIT_RAW_BLOCK_LOCK(rb) pthread_mutex_init(&rb->mutex, NULL);
/** Unlocks a specific raw block */
#define UNLOCK_RAW_BLOCK(rb) pthread_mutex_unlock(&rb->mutex)

#else /* HAVE_LOCKS */

/** Does nothing. */
#define LOCK_GLOBAL()
/** Does nothing. */
#define UNLOCK_GLOBAL()
/** Does nothing. */
#define LOCK_RAW_BLOCK(RB)
/** Does nothing. */
#define INIT_RAW_BLOCK_LOCK(RB)
/** Does nothing. */
#define UNLOCK_RAW_BLOCK(RB)

#endif /* HAVE_LOCKS */

#endif /* LOCKS_H */
