/*
 *   Copyright Institute of Communication and Computer Systems (ICCS) 
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 */

/**
 * @file   dmmlib_trace.h
 * @author Ioannis Koutras (joko@microlab.ntua.gr)
 * @date   September 2012
 * @brief  Tracing functions
 */

#ifndef _DMMLIB_TRACE_H_
#define _DMMLIB_TRACE_H_
#include "dmmlib/config.h"

#ifdef PARSE_ENV
#include "dmmlib/dmmlib.h" /* to check if systemallocator is initialized */
#endif /* PARSE_ENV */

#ifdef WITH_MEM_TRACE
#include <stdio.h>

#ifndef PARSE_ENV
/** Memory trace file descriptor */
#define MEM_FD stderr

/** Function for memory trace messages */
#define MEM_TRACE(...) fprintf(MEM_FD, __VA_ARGS__)

#else /* PARSE_ENV */
/** Memory trace file descriptor macro */
#define MEM_FD mem_fd
/** Memory trace file descriptor */
FILE* mem_fd;

/** Function for memory trace messages */
#define MEM_TRACE(...) \
    if(__builtin_expect (systemallocator.initialized, true)) { \
        fprintf(MEM_FD, __VA_ARGS__); \
    }

#endif /* PARSE_ENV */

#else /* WITH_MEM_TRACE */

/** Function for memory trace messages */
#define MEM_TRACE(...) /* do nothing */

#endif /* WITH_MEM_TRACE */

#ifdef WITH_STATS_TRACE
#include <stdio.h>

#ifndef PARSE_ENV
/** Statistics file descriptor */
#define STATS_FD stderr

/** Function for statistics trace messages */
#define STATS_TRACE(...) fprintf(STATS_FD, __VA_ARGS__)

#else /* PARSE_ENV */
/** Statistics file descriptor macro */
#define STATS_FD stats_fd
/** Statistics file descriptor */
FILE* stats_fd;

/** Function for statistics trace messages */
#define STATS_TRACE(...) \
    if(__builtin_expect (systemallocator.initialized, true)) { \
        fprintf(STATS_FD, __VA_ARGS__); \
    }

#endif /* PARSE_ENV */

#else /* WITH_STATS_TRACE */

/** Function for statistics trace messages */
#define STATS_TRACE(...) /* do nothing */

#endif /* WITH_STATS_TRACE */

#ifdef WITH_DEBUG
#include <stdio.h>

#ifndef PARSE_ENV
/** Debug file descriptor */
#define DBG_FD stderr

/** Function for debug trace messages */
#define DBG_TRACE(...) fprintf(DBG_FD, __VA_ARGS__)

#else /* PARSE_ENV */
/** Debug file descriptor macro */
#define DBG_FD dbg_fd
/** Debug file descriptor */
FILE* dbg_fd;

/** Function for debug trace messages */
#define DBG_TRACE(...) \
    if(__builtin_expect (systemallocator.initialized, true)) { \
        fprintf(DBG_FD, __VA_ARGS__); \
    }

#endif /* PARSE_ENV */

#else /* WITH_DEBUG */

/** Function for debug trace messages */
#define DBG_TRACE(...) /* do nothing */

#endif /* WITH_DEBUG */

#endif /* _DMMLIB_TRACE_H_ */
