/*
 *   Copyright 2012 Institute of Communication and Computer Systems (ICCS) 
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 */

/**
 * \file 	block_header_funcs.h
 * \author 	Ioannis Koutras (joko@microlab.ntua.gr)
 *  
 * \brief Memory block functions.
 */

#ifndef BLOCK_HEADER_FUNCS_H
#define BLOCK_HEADER_FUNCS_H

#include "dmmlib/freelist/block_header.h"
#include "dmmlib/freelist/freelist_rb.h"

/**
 * \brief Get the address of the block header of a memory block.
 *
 * \param ptr The data part of the memory block.
 */
block_header_t * get_header(void *ptr);

/**
 * Get the size of the memory block's data
 *
 * \param ptr The pointer to the current memory block.
 *
 * \return The size of the data part of the current memory block.
 */
size_t get_size(block_header_t *ptr);

#ifdef REQUEST_SIZE_INFO

/** Macro to set the requested size to the header of a free-list's memory
 * block */
#define SET_REQUESTED_SIZE(ptr, size) set_requested_size(ptr, size)

/**
 * Set the requested size of memory block's data
 *
 * \param ptr 	The pointer to the data part of the current memory block.
 * \param size 	The requested size for the data part of the current memory
 * block.
 */
void set_requested_size(block_header_t *ptr, size_t size);

/**
 * Get the requested size of the memory block's data
 *
 * \param ptr The pointer to the data part of the current memory block.
 *
 * \return The size of the data that was initialy requested for this memory
 * block.
 */
size_t get_requested_size(block_header_t *ptr);
#else /* REQUEST_SIZE_INFO */

/** Does nothing */
#define SET_REQUESTED_SIZE(...)

#endif /* REQUEST_SIZE_INFO */

/**
 * Get all information of the memory block header's size record
 *
 * \param ptr The pointer to the data part of the current memory block.
 *
 * \return The availability and the size of the data part of the current memory
 * block.
 */
size_t get_size_availability(block_header_t *ptr);

/**
 * Set the size of the memory block's data and mark it free
 *
 * \param raw_block The pointer to the raw block which includes the block.
 * \param ptr 	The pointer to the data part of the current memory block.
 * \param size 	The size of the data part of the current memory block.
 */
void set_size_and_free(freelist_rb_t *raw_block, block_header_t *ptr, size_t size);

/**
 * Set the size of the memory block's data and mark it used
 *
 * \param raw_block The pointer to the raw block which includes the block.
 * \param ptr 	The pointer to the data part of the current memory block.
 * \param size 	The size of the data part of the current memory block.
 */
void set_size_and_used(freelist_rb_t *raw_block, block_header_t *ptr, size_t size);

/**
 * Mark the memory block as used, as well as the previous_size element of the
 * next block if there is one.
 *
 * \param raw_block The pointer to the raw block which includes the block.
 * \param ptr 	The pointer to the data part of the memory block.
 */
void mark_used(freelist_rb_t *raw_block, block_header_t *ptr);

/**
 * Mark the memory block as free, as well as the previous_size element of the
 * next block if there is one.
 *
 * \param raw_block The pointer to the raw block which includes the block.
 * \param ptr 	The pointer to the data part of the memory block.
 */
void mark_free(freelist_rb_t *raw_block, block_header_t *ptr);

/**
 * Set the availability and the size of the previous memory block
 *
 * \param ptr 		The pointer to the data part of the previous memory block.
 * \param previous_size_availability The size for the data part of the previous
 * memory block on data layout level.
 */
void set_previous_size_availability(block_header_t *ptr, size_t previous_size_availability);

/**
 * Check if a memory block is free
 */
bool is_free(block_header_t *ptr);

/**
 * Check if previous block (in the memory space) belongs to a free list
 */
bool is_previous_free(block_header_t *ptr);

/**
 * Get the size of the previous block (in the memory space) 
 *
 * \param ptr 	The pointer to the data part of the current memory block.
 */
size_t get_previous_size(block_header_t *ptr);

/**
 * Get the size and the availability of the previous block (in the memory
 * space) 
 *
 * \param ptr 	The pointer to the data part of the previous memory block.
 */
size_t get_previous_size_availability(block_header_t *ptr);

/**
 * Get the previous memory block on data layout level
 *
 * \param ptr 	The pointer to the data part of the current memory block.
 *
 * \return 	The pointer to the data part of the previous memory block on
 * data layout level. 
 */
block_header_t * get_dlprevious(block_header_t *ptr);

/**
 * Get the next memory block on data layout level if there is one
 *
 * \param raw_block     The pointer to the raw block which includes the block.
 * \param ptr 		The pointer to the data part of the current memory block.
 *
 * \return The pointer to the data part of the next block.
 * \retval NULL There is no next block.
 */
block_header_t * get_dlnext(freelist_rb_t *raw_block, block_header_t *ptr);

#endif /* BLOCK_HEADER_FUNCS_H */
