/*
 *   Copyright 2011 Institute of Communication and Computer Systems (ICCS) 
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 */

#ifndef COALESCE_H
#define COALESCE_H
#include <dmmlib/config.h>

#include "dmmlib/freelist/freelist_rb.h"
#include "dmmlib/freelist/block_header.h"

/**
 * Tries to merges a memory block with its previous one and/or the next one.
 *
 * @param raw_block The raw block owner.
 * @param ptr The memory block to be checked.
 */
void coalesce(freelist_rb_t *raw_block, block_header_t *ptr);

#endif /* COALESCE_H */
