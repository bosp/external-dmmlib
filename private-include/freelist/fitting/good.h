/*
 *   Copyright Institute of Communication and Computer Systems (ICCS) 
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 */

/**
 * \file        good.h
 * \author 	Ioannis Koutras (joko@microlab.ntua.gr)
 * \date        October 2012
 *  
 * \brief Good-fit search algorithm for freelist-organized raw blocks.
 */

#ifndef FL_GOOD_FIT_H
#define FL_GOOD_FIT_H
#include "dmmlib/freelist/freelist_rb.h"
#include "dmmlib/freelist/block_header.h"

/**
 * Performs a good-fit search on a list for a block of a certain size
 *
 * \param raw_block      The freelist-organized raw block.
 * \param requested_size The desired size of the block.
 *
 * \return The pointer to the matched memory block.
 * \retval NULL No block was found.
 */
block_header_t * fl_good_fit(freelist_rb_t *raw_block, size_t requested_size);

#endif /* GOOD_FIT_H */
