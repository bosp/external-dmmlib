/*
 *   Copyright Institute of Communication and Computer Systems (ICCS) 
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 */

/**
 * \file        freelist/fitting_policy.h
 * \author 	Ioannis Koutras (joko@microlab.ntua.gr)
 * \date        October 2012
 *  
 * \brief Defines the default fitting policy in freelist-organized raw blocks.
 */

#ifndef FL_FITTING_POLICY_H
#define FL_FITTING_POLICY_H

#include <dmmlib/config.h>

/**
 * @def SEARCH_LIST
 * Tries to find an available free memory block by using the default search
 * method.
 *
 * @param raw_block The freelist-organized raw block
 * @param size      The requested size
 */

#if defined (BEST_FIT)
#include "freelist/fitting/best.h"
#define SEARCH_LIST(raw_block, size) fl_best_fit(raw_block, size)
#elif defined (EXACT_FIT)
#include "freelist/fitting/exact.h"
#define SEARCH_LIST(raw_block, size) fl_exact_fit(raw_block, size)
#elif defined (FIRST_FIT)
#include "freelist/fitting/first.h"
#define SEARCH_LIST(raw_block, size) fl_first_fit(raw_block, size)
#elif defined (GOOD_FIT)
#include "freelist/fitting/good.h"
#define SEARCH_LIST(raw_block, size) fl_good_fit(raw_block, size)
#endif /* BEST_FIT */ 

#endif /* FL_FITTING_POLICY_H */
