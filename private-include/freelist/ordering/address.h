/*
 *   Copyright 2011 Institute of Communication and Computer Systems (ICCS) 
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 */

/**
 * \file        address.h
 * \author      Ioannis Koutras (joko@microlab.ntua.gr)
 * \date        October 2012
 *  
 * \brief Add a block in an address-ordered linked list.
 */

#ifndef FL_ADDRESS_ORDER_H
#define FL_ADDRESS_ORDER_H

#include "dmmlib/freelist/block_header.h"

/**
 * Adds a block in an address-ordered list.
 *
 * @param head  The head of the list.
 * @param block The memory block to be added.
 */
void add_to_address_ordered(struct fl_head_s *head, block_header_t *block);

#endif /* FL_ADDRESS_ORDER_H */

