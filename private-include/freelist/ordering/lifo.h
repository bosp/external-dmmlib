/*
 *   Copyright 2011 Institute of Communication and Computer Systems (ICCS) 
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 */

/**
 * \file        size.h
 * \author      Ioannis Koutras (joko@microlab.ntua.gr)
 * \date        October 2012
 *  
 * \brief Add a block in a LIFO-ordered linked list.
 */

#ifndef FL_LIFO_ORDER_H
#define FL_LIFO_ORDER_H

#include "dmmlib/freelist/freelist_rb.h"
#include "dmmlib/freelist/block_header.h"

/**
 * Adds a block in a LIFO-ordered list.
 *
 * @param raw_block The raw block.
 * @param block     The memory block to be added.
 */
void add_to_lifo_ordered(freelist_rb_t *raw_block, block_header_t *block);

#endif /* FL_LIFO_ORDER_H */

