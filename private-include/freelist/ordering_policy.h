/*
 *   Copyright Institute of Communication and Computer Systems (ICCS) 
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 */

/**
 * \file        freelist/ordering_policy.h
 * \author 	Ioannis Koutras (joko@microlab.ntua.gr)
 * \date        October 2012
 *  
 * \brief Defines the default ordering policy in freelist-organized raw blocks.
 */

#ifndef FL_ORDERING_POLICY_H
#define FL_ORDERING_POLICY_H

#include <dmmlib/config.h>

#include "dmmlib/lists.h"

/**
 * @def ADD_BLOCK
 * Adds a block to the list of free memory blocks by using the default ordering
 * policy.
 *
 * @param block The block to be added.
 */

#if defined (ADDRESS_ORDERED)
#include "freelist/ordering/address.h"
#define ADD_BLOCK(block) add_to_address_ordered(&raw_block->fl_head, block)

#elif defined (FIFO_ORDERED)
#include "freelist/ordering/fifo.h"
#define ADD_BLOCK(block) add_to_fifo_ordered(raw_block, block)

#elif defined (LIFO_ORDERED)
#include "freelist/ordering/lifo.h"
#define ADD_BLOCK(block) add_to_lifo_ordered(raw_block, block)

#elif defined (SIZE_ORDERED)
#include "freelist/ordering/size.h"
#define ADD_BLOCK(block) add_to_size_ordered(&raw_block->fl_head, block)

#endif /* ADDRESS_ORDERED */


/**
 * @def REMOVE_FSLLIST_HEAD
 * Removes the top block of the list of free memory blocks.
 *
 * @param raw_block The pointer to the freelist-organized raw block.
 */

/**
 * @def REMOVE_FSLLIST
 * Removes a memory block from the list of free memory blocks.
 *
 * @param raw_block    The pointer to the freelist-organized raw block.
 * @param memory_block The pointer to the memory block to be removed.
 */

#ifndef FIFO_ORDERED
#define REMOVE_FSLLIST_HEAD(raw_block)                \
    SLIST_REMOVE_HEAD(&raw_block->fl_head, pointers);

#define REMOVE_FSLLIST(raw_block, memory_block)                            \
SLIST_REMOVE(&raw_block->fl_head, memory_block, block_header_s, pointers);

#else /* FIFO_ORDERED */
#define REMOVE_FSLLIST_HEAD(raw_block)                       \
    if(raw_block->fl_tail == raw_block->fl_head.slh_first) { \
        raw_block->fl_tail = NULL;                           \
    }                                                        \
    SLIST_REMOVE_HEAD(&raw_block->fl_head, pointers);

#define REMOVE_FSLLIST(raw_block, memory_block) do {               \
    if ((&raw_block->fl_head)->slh_first == memory_block) {        \
        REMOVE_FSLLIST_HEAD(raw_block);                            \
    } else {						           \
        block_header_t *curelm = (&raw_block->fl_head)->slh_first; \
        while(curelm->pointers.sle_next != memory_block)           \
            curelm = curelm->pointers.sle_next;                    \
        if(raw_block->fl_tail == curelm->pointers.sle_next) {      \
            raw_block->fl_tail = curelm;                           \
        }                                                          \
        curelm->pointers.sle_next =                                \
            curelm->pointers.sle_next->pointers.sle_next;          \
    }							           \
} while (/*CONSTCOND*/0)

#endif /* FIFO_ORDERED */

#endif /* FL_ORDERING_POLICY_H */
