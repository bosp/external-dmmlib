/*
 *   Copyright Institute of Communication and Computer Systems (ICCS) 
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 */

/**
 * @file   default_rb.h
 * @author Ioannis Koutras (joko@microlab.ntua.gr)
 * @date   October 2012
 *
 * @brief  Include libraries and defines for default raw block selection.
 */

#ifndef DEFAULT_RB_H
#define DEFAULT_RB_H

#include <dmmlib/config.h>

#ifdef BITMAP_RB_ONLY
#include "bitmap/bitmap.h"
#include "bitmap/bitmap_rb.h"
/** Default raw block data type */
#define DEFAULT_RB_T bitmap_rb_t
/** Default raw block enumeration type */
#define DEFAULT_RB_TYPE BITMAP
#endif /* BITMAP_RB_ONLY */

#ifdef FL_RB_ONLY
#include "dmmlib/freelist/freelist.h"
#include "dmmlib/freelist/freelist_rb.h"
/** Default raw block data type */
#define DEFAULT_RB_T freelist_rb_t
/** Default raw block enumeration type */
#define DEFAULT_RB_TYPE FREELIST
#endif /* FL_RB_ONLY */

#endif /* DEFAULT_RB_H */
