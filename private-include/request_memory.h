/*
 *   Copyright 2012 Institute of Communication and Computer Systems (ICCS) 
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 */

/**
 * \file   request_memory.h
 * \author Ioannis Koutras
 * \date   August, 2012
 * \brief  Function prototype to request additional memory space.
 */

#ifndef REQUEST_MEMORY_H
#define REQUEST_MEMORY_H

#include <stddef.h> /* for size_t */

/**
 * Requests memory address space from the OS.
 *
 * @param size The memory space to be requested in bytes.
 *
 * @return A pointer to the memory space.
 * @retval NULL The OS could not return any memory space.
 */
void * request_memory(size_t size);

#endif /* REQUEST_MEMORY_H */
