/*
 *   Copyright Institute of Communication and Computer Systems (ICCS) 
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 */

/**
 * @file   release_memory.h
 * @author Ioannis Koutras
 * @date   September 2012
 * @brief  Function prototype to release memory space to the system.
 */

#ifndef RELEASE_MEMORY_H
#define RELEASE_MEMORY_H

#include "dmmlib/raw_block.h"

/**
 * Releases the memory space of a raw block back to the OS.
 *
 * @param raw_block The pointer of the raw block which is to be released.
 */
void release_memory(raw_block_header_t *raw_block);

#endif /* RELEASE_MEMORY_H */
