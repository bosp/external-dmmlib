/*
 *   Copyright Institute of Communication and Computer Systems (ICCS) 
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 */

/**
 * @file   statistics.h
 * @author Ioannis Koutras (joko@microlab.ntua.gr)
 * @date   September 2012
 * @brief  Statistics function.
 */

#ifndef DMMLIB_STATISTICS_H_
#define DMMLIB_STATISTICS_H_
#include <dmmlib/config.h>

#ifdef WITH_ALLOCATOR_STATS

#include <inttypes.h>

#include "dmmlib/dmmlib.h"
#include "dmmlib/dmmstats.h"
#include "locks.h"

#ifdef REQUEST_SIZE_INFO
/** Updates the global statistics for specific dmmlib events */
#define UPDATE_GLOBAL_STATS(EVENT_TYPE, REQ_SIZE) \
    LOCK_GLOBAL(); \
    update_stats(&systemallocator.dmm_stats, EVENT_TYPE, REQ_SIZE); \
    UNLOCK_GLOBAL();
#else /* REQUEST_SIZE_INFO */
/** Updates the global statistics for specific dmmlib events */
#define UPDATE_GLOBAL_STATS(EVENT_TYPE) \
    LOCK_GLOBAL(); \
    update_stats(&systemallocator.dmm_stats, EVENT_TYPE); \
    UNLOCK_GLOBAL();
#endif /* REQUEST_SIZE_INFO */

/** DMM event type enumeration */
typedef enum dmm_event_type_en
    { MALLOC
    , FREE
#ifdef WITH_REALLOC
    , REALLOC_LT
    , REALLOC_GT
#endif /* WITH_REALLOC */
#ifdef WITH_MEMALIGN
    , MEMALIGN
#endif /* WITH_MEMALIGN */
} dmm_event_type;

void update_stats
  ( dmmstats_t *dmm_stats
  , dmm_event_type event
#ifdef REQUEST_SIZE_INFO
  , size_t mem_req
#endif /* REQUEST_SIZE_INFO */
  );

#else /* WITH_ALLOCATOR_STATS */

/** Does nothing */
#define UPDATE_GLOBAL_STATS(...)

#endif /* WITH_ALLOCATOR_STATS */

#endif /* DMMLIB_STATISTICS_H_ */
