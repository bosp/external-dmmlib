#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <dmmlib/initialize_allocator.h>
#include <dmmlib/dmmlib.h>
#include <dmmlib/config.h>

#define ALLOCATOR_SIZE 2*MAX_COAL_SIZE*sizeof(char)

#define cmalloc(size) custom_ahmalloc(&systemallocator, &systemallocator.heaps[0], (size_t) size)
#define cfree(ptr) custom_ahfree(&systemallocator, &systemallocator.heaps[0], ptr)
#ifdef WITH_REALLOC
#define crealloc(ptr, size) custom_ahrealloc(&systemallocator, &systemallocator.heaps[0], ptr, size)
#endif /* WITH_REALLOC */

int main(void) {

    allocator_t systemallocator;
    void *block_from_malloc;
    void *p1, *p2, *p3;

    block_from_malloc = malloc(ALLOCATOR_SIZE);    
    initialize_allocator(&systemallocator, block_from_malloc, ALLOCATOR_SIZE);

    /* Testing a best-fit, w/o fixed lists allocator */

    /* Test 1: Check is a block can be reused if free'd before. */
    printf("Test 1: Check is a block can be reused if free'd before.\n");
    p1 = cmalloc(1024);
    cfree(p1);
    /* Check if the block is taken back */
    p2 = cmalloc(1024);
    assert( p2 == p1 );
    cfree(p2);
    printf("Test 1: Success.\n");

    /* Test 2: Check if best fit on LIFO works */
    printf("Test 2: Check if best fit on LIFO works.\n");
    /* Allocate two memory blocks of 1024 and 432 bytes */
    p1 = cmalloc(MAX_COAL_SIZE);
    p2 = cmalloc(432);
    /* Make them free */
    cfree(p2);
    cfree(p1);
    /* Pray that the following malloc is going to use the second block of the
     * free list.
     */
    p3 = cmalloc(432);
    assert( p3 == p2 );
    cfree(p3);
    printf("Test 2: Success.\n");

    /* Test 3: Check if splitting works
     *
     * Note: Splitting is checked for fixed-sized splitting or the
     * initial value of variable-sized splitting.
     */
#ifdef MIN_SPLIT_SIZE
    printf("Test 3: Check if splitting works.\n");
    /* Reset everything */
    initialize_allocator(&systemallocator, block_from_malloc, ALLOCATOR_SIZE);    
    if(1000 - MIN_SPLIT_SIZE > 0 && MIN_SPLIT_SIZE - 50 > 0) {
        p1 = cmalloc(1000);
        cfree(p1);
        /* This would go to the first block and split it */
        p1 = cmalloc(1000 - MIN_SPLIT_SIZE - 50); /* 50 to compansate safely the
                                                         header size */
        /* This goes to the third block */
        p2 = cmalloc(432);
        /* Check if the split free block is used */
        p3 = cmalloc(MIN_SPLIT_SIZE - 50); /* 50 to compansate safely the
                                                  header size */
        assert( p3 > p1 ); /* Small sanity check */
        assert( p3 < p2 );
        printf("Test 3: Success.\n");
    } else {
        printf("Please check maximum splitting size and try to fit it between 50 and 1000 bytes.\n");
    }
#endif /* MIN_SPLIT_SIZE */

#ifdef WITH_REALLOC
    /* Testing realloc() */
    printf("Testing realloc()...\n");
    /* Reset everything */
    initialize_allocator(&systemallocator, block_from_malloc, ALLOCATOR_SIZE);

    /* realloc() acts as malloc() if given pointer is null */
    p1 = crealloc(NULL, 1000);
    assert( p1 != NULL );

    /* The new size is smaller than the original */
    p2 = crealloc(p1, 50);
    assert( p2 == p1 );

    /* Original size smaller than newer, non-free next block */
    /* Reset everything */
    initialize_allocator(&systemallocator, block_from_malloc, ALLOCATOR_SIZE);
    p1 = cmalloc(100);
    p2 = cmalloc(100);
    p3 = crealloc(p1, 1000);
    assert( p3 > p2 );

    /* Original size smaller than newer, free next block */
    /* Reset everything */
    initialize_allocator(&systemallocator, block_from_malloc, ALLOCATOR_SIZE);
#if defined (SPLITTING_FIXED) || defined (SPLITTING_VARIABLE)
    p1 = cmalloc(MIN_SPLIT_SIZE);
    p2 = cmalloc(2*MIN_SPLIT_SIZE);
    cfree(p2);
    p3 = crealloc(p1, 2*MIN_SPLIT_SIZE);
    p2 = cmalloc(MIN_SPLIT_SIZE);
    /* FIXME 24 is the current size of the block header */
    assert( (char *) p2 == (char *) p1 + 24 + 2 * MIN_SPLIT_SIZE);
#else /* SPLITTING_FIXED || SPLITTING_VARIABLE */
    p1 = cmalloc(100);
    p2 = cmalloc(100);
    cfree(p2);
    p3 = crealloc(p1, 200);
#endif /* SPLITTING_FIXED || SPLITTING_VARIABLE */
    assert( p3 == p1 );

    printf("Passed.\n");
#endif /* WITH_REALLOC */

    free(block_from_malloc);

    return 0;
}
